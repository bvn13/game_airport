PGDMP     +    '    	            q            mg    9.1.8    9.1.8 �    t
           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            u
           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            v
           1262    17418    mg    DATABASE     t   CREATE DATABASE mg WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'ru_RU.UTF-8' LC_CTYPE = 'ru_RU.UTF-8';
    DROP DATABASE mg;
             game    false                        2615    17419    mgdata    SCHEMA        CREATE SCHEMA mgdata;
    DROP SCHEMA mgdata;
             game    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            w
           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    5            x
           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    5            �            3079    25631    plperl 	   EXTENSION     >   CREATE EXTENSION IF NOT EXISTS plperl WITH SCHEMA pg_catalog;
    DROP EXTENSION plperl;
                  false            y
           0    0    EXTENSION plperl    COMMENT     >   COMMENT ON EXTENSION plperl IS 'PL/Perl procedural language';
                       false    195            �            3079    25636    plperlu 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plperlu WITH SCHEMA pg_catalog;
    DROP EXTENSION plperlu;
                  false            z
           0    0    EXTENSION plperlu    COMMENT     J   COMMENT ON EXTENSION plperlu IS 'PL/PerlU untrusted procedural language';
                       false    194            �            3079    12223    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            {
           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    196            �            1255    25831 �   add_new_quest(character varying, character varying, smallint, character varying, character varying, character varying, character varying)    FUNCTION     �  CREATE FUNCTION add_new_quest(OUT "OUT" boolean, text character varying, text_ru character varying, duration smallint, conditions character varying, onstart character varying, onsuccess character varying, onfail character varying) RETURNS boolean
    LANGUAGE plperlu
    AS $_$



use strict;
use warnings;

use Mojo::JSON;

my ($text, $text_ru, $duration, $j_cond, $j_onStart, $j_onSuccess, $j_onFail) = @_;

spi_exec_query("BEGIN");

###############################
# QUEST
###############################
my $rv_q = spi_exec_query(<<EOQ

    INSERT INTO mgdata.quests_catalog(text, text_ru, duration)
    VALUES ('$text', '$text_ru', '$duration')
    RETURNING id;

EOQ
);

if ($rv_q->{error}) {
    elog(ERROR, $rv_q->{error});
    return 0;
}

my $qid = $rv_q->{rows}->[0]->{id};

my $j = Mojo::JSON->new;

##################################
# CONDITION
##################################
my $cond = $j->decode($j_cond);
do {elog(ERROR, $j->error); return 0;} if $j->error;

my $q_cond = '';

for my $k (keys %$cond) {
    my $v = $cond->{$k};

    $q_cond .= <<EOQ

    INSERT INTO mgdata.quests_conditions_by_profile(quest_id, profile_parameter_id, value)
    VALUES ('$qid', '$k', '$v')
    ;

EOQ

}

elog(INFO, "\n" . $q_cond);
my $rv_qc = spi_exec_query($q_cond);
do { elog(ERROR, $rv_qc->{error}); return 0; } if $rv_qc->{error};

#################################
# ON START
#################################
my $onStart = $j->decode($j_onStart);
do {elog(ERROR, $j->error); return 0;} if $j->error;

my $q_onStart = '';

for my $k (keys %$onStart) {
    my $v = $onStart->{$k};

    $q_onStart .= <<EOQ

    INSERT INTO mgdata.quests_onstart_actions_by_profile(quest_id, profile_parameter_id, change)
    VALUES ('$qid', '$k', '$v')
    ;

EOQ

}

elog(INFO, "\n" . $q_onStart);
my $rv_qost = spi_exec_query($q_onStart);
do { elog(ERROR, $rv_qost->{error}); return 0; } if $rv_qost->{error};

#################################
# ON SUCCESS
#################################
my $onSuc = $j->decode($j_onSuccess);
do {elog(ERROR, $j->error); return 0;} if $j->error;

my $q_onSuc = '';

for my $k (keys %$onSuc) {
    my $v = $onSuc->{$k};

    $q_onSuc .= <<EOQ

    INSERT INTO mgdata.quests_onsuccess_actions_by_profile(quest_id, profile_parameter_id, change)
    VALUES ('$qid', '$k', '$v')
    ;

EOQ

}

elog(INFO, "\n" . $q_onSuc);
my $rv_qosuc = spi_exec_query($q_onSuc);
do { elog(ERROR, $rv_qosuc->{error}); return 0; } if $rv_qosuc->{error};

#################################
# ON START
#################################
my $onFail = $j->decode($j_onFail);
do {elog(ERROR, $j->error); return 0;} if $j->error;

my $q_onFail = '';

for my $k (keys %$onFail) {
    my $v = $onStart->{$k};

    $q_onFail .= <<EOQ

    INSERT INTO mgdata.quests_onfail_actions_by_profile(quest_id, profile_parameter_id, change)
    VALUES ('$qid', '$k', '$v')
    ;

EOQ

}

elog(INFO, "\n" . $q_onFail);
my $rv_qof = spi_exec_query($q_onFail);
do { elog(ERROR, $rv_qof->{error}); return 0; } if $rv_qof->{error};



spi_exec_query("END;");

return 1;



$_$;
 �   DROP FUNCTION mgdata.add_new_quest(OUT "OUT" boolean, text character varying, text_ru character varying, duration smallint, conditions character varying, onstart character varying, onsuccess character varying, onfail character varying);
       mgdata       game    false    610    7            �            1255    42026    clear_user_quests(bigint)    FUNCTION     �   CREATE FUNCTION clear_user_quests(user_id bigint) RETURNS void
    LANGUAGE sql
    AS $_$DELETE FROM mgdata.users_quests WHERE user_id = $1;$_$;
 8   DROP FUNCTION mgdata.clear_user_quests(user_id bigint);
       mgdata       game    false    7            �            1255    41993 :   create_new_user_object(integer, integer, integer, integer)    FUNCTION     �  CREATE FUNCTION create_new_user_object(OUT "OUT" integer, user_id integer, object_id integer, coord_x integer, coord_y integer) RETURNS integer
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;
use Encode qw/decode/;

my ($uid, $oid, $cx1, $cy1) = @_;

my $qtest = <<EOQ;

SELECT
    uo.user_id, uo.object_id, uo.coord_x, uo.coord_y,
    oc.area_x, oc.area_y,
    uo.coord_x+oc.area_x-1 AS coord_area_x,
    uo.coord_y+oc.area_y-1 AS coord_area_y
  FROM mgdata.users_objects uo
  INNER JOIN mgdata.objects_catalog oc
    ON uo.object_id = oc.id
  INNER JOIN (
    SELECT 
      userdata.user_id, 
      (userdata.x1+sets.user_begin_coord_x)::integer AS x1,
      (userdata.x1+cat.area_x+sets.user_begin_coord_x-1)::integer AS x2,
      (userdata.y1+sets.user_begin_coord_y)::integer AS y1,
      (userdata.y1+cat.area_y+sets.user_begin_coord_y-1)::integer AS y2,
      sets.map_x, sets.map_y, sets.user_begin_coord_x, sets.user_begin_coord_x
    FROM (
	    SELECT ($uid)::integer user_id,
	      ($cx1)::integer x1, ($cy1)::integer y1
    ) AS userdata
    LEFT JOIN mgdata.settings_str AS sets
    ON TRUE
    LEFT JOIN mgdata.objects_catalog as cat
    ON cat.id = $oid
  ) AS data
    ON data.user_id = uo.user_id
    AND
    (
      ( -- 1
          data.x1 >= uo.coord_x AND data.x1 <= uo.coord_x+oc.area_x-1  -- случай, когда левый верхний угол в поле объекта
      AND data.y1 >= uo.coord_y AND data.y1 <= uo.coord_y+oc.area_y-1
      ) OR ( -- 2
          data.x2 >= uo.coord_x AND data.x2 <= uo.coord_x+oc.area_x-1  -- случай, когда правый нижний угол в поле объекта
      AND data.y2 >= uo.coord_y AND data.y2 <= uo.coord_y+oc.area_y-1
      ) OR ( -- 3
          data.x1 >= uo.coord_x AND data.x1 <= uo.coord_x+oc.area_x-1  -- когда левый нижний в поле
      AND data.y2 >= uo.coord_y AND data.y2 <= uo.coord_y+oc.area_y-1
      ) OR ( -- 4
          data.x2 >= uo.coord_x AND data.x2 <= uo.coord_x+oc.area_x-1  -- когда правый нижний в поле
      AND data.y1 >= uo.coord_y AND data.y1 <= uo.coord_y+oc.area_y-1
      ) OR ( -- 5
	  data.x1 <= uo.coord_x AND data.x2 >= uo.coord_x+oc.area_x-1  -- когда обхватывает собой объект
      AND data.y1 <= uo.coord_y AND data.y2 >= uo.coord_y+oc.area_y-1
      )
    )
WHERE uo.object_id <> 3 AND uo.object_id <> 14  -- можно на бетоне (14) и захваченной земле (3)
EOQ

my $rvtest = spi_exec_query($qtest);
if ($rvtest->{error}) {
    elog(ERROR, decode('utf8', $rvtest->{error}));
    return -1;
}

if (scalar(@{$rvtest->{rows}}) > 0) {
    return -2;
}


my $q = <<EOQ;

INSERT INTO mgdata.users_objects(user_id, object_id, coord_x, coord_y)
VALUES('$uid', '$oid', '$cx1', '$cy1')
RETURNING id
;

EOQ


my $rv = spi_exec_query($q);
if ($rv->{error}) {
    elog(ERROR, decode('utf8', $rv->{error}));
    return -3;
}

return $rv->{rows}->[0]->{id};



$_$;
 �   DROP FUNCTION mgdata.create_new_user_object(OUT "OUT" integer, user_id integer, object_id integer, coord_x integer, coord_y integer);
       mgdata       game    false    7    610            �            1255    42024 )   end_user_quest_with_fail(bigint, integer)    FUNCTION     {
  CREATE FUNCTION end_user_quest_with_fail(OUT "OUT" integer, uid bigint, qid integer) RETURNS integer
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;

my ($uid, $qid) = @_;

my $q = <<EOQ;

    SELECT *, 
    datebegin + (cast(duration AS varchar) || ' sec')::interval AS dateendestimated,
    CASE 
      WHEN datebegin + (cast(duration AS varchar) || ' sec')::interval > now() 
      THEN false 
      ELSE true 
    END AS available
    FROM mgdata.users_quests_started
    WHERE user_id = '$uid' AND quest_id = '$qid'
    AND dateend IS NULL

EOQ

my $r = spi_exec_query($q);
if ($r->{error}) {
    elog(ERROR, $r->{error});
    return -1;
}

if (scalar(@{$r->{rows}}) == 0) {
    elog(ERROR, $r->{error});
    return -2;
}

if ($r->{rows}->[0]->{available} ne 't') {
    return 0;
}

spi_exec_query('BEGIN;');

my $q_selcond = <<EOQ;

    SELECT act.profile_parameter_id, act.change, act.is_delta, prf.value
    FROM mgdata.quests_onfail_actions_by_profile AS act
    LEFT JOIN mgdata.users_profiles AS prf
    ON prf.parameter_id = act.profile_parameter_id
    AND act.quest_id = '$qid' 
    AND prf.user_id = '$uid'
    
EOQ

my $rv_selcond = spi_exec_query($q_selcond);
if ($rv_selcond->{error}) {
    elog(ERROR, $rv_selcond->{error});
    spi_exec_query('ROLLBACK;');
    return -3;
}

my $q_apply = '';

for my $row (@{$rv_selcond->{rows}}) {
    if ($row->{is_delta}) {
        $q_apply .= <<EOC;

            UPDATE mgdata.users_profiles
            SET value = value::integer + $row->{change}
            WHERE parameter_id = $row->{profile_parameter_id}
            AND user_id = '$uid'
            ;

EOC
    } else {
        $q_apply .= <<EOC;

            UPDATE mgdata.users_profiles
            SET value = $row->{change}
            WHERE parameter_id = $row->{profile_parameter_id}
            AND user_id = '$uid'
            AND value::integer > $row->{change}  -- менять надо, если текущее значение больше того, на которое меняем
            -- иначе можно поменять в меньшую сторону
            ;

EOC
    }    

}

my $rv_apply = spi_exec_query($q_apply);
if ($rv_apply->{error}) {
    elog(ERROR, "QUERY: \n" . $q_apply . "\n\n" . $rv_apply->{error});
    spi_exec_query('ROLLBACK;');
    return -4;
}

my $q_start = <<EOQ;

    UPDATE mgdata.users_quests
    SET dateend = NOW(), success = FALSE
    WHERE user_id = '$uid'
    AND quest_id = '$qid'

EOQ

my $rv_start = spi_exec_query($q_start);
if ($rv_start->{error}) {
    elog(ERROR, $rv_start->{error});
    spi_exec_query('ROLLBACK;');
    return -5;
}

spi_exec_query('COMMIT;');


return 1;


$_$;
 [   DROP FUNCTION mgdata.end_user_quest_with_fail(OUT "OUT" integer, uid bigint, qid integer);
       mgdata       game    false    610    7            �            1255    42023 ,   end_user_quest_with_success(bigint, integer)    FUNCTION       CREATE FUNCTION end_user_quest_with_success(OUT "OUT" integer, uid bigint, qid integer) RETURNS integer
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;

use Data::Dumper;

my ($uid, $qid) = @_;

my $q = <<EOQ;

    SELECT *, 
    datebegin + (cast(duration AS varchar) || ' sec')::interval AS dateendestimated,
    CASE 
      WHEN datebegin + (cast(duration AS varchar) || ' sec')::interval > now() 
      THEN false 
      ELSE true 
    END AS available
    FROM mgdata.users_quests_started
    WHERE user_id = '$uid' AND quest_id = '$qid'
    AND dateend IS NULL

EOQ

my $r = spi_exec_query($q);
if ($r->{error}) {
    elog(ERROR, $r->{error});
    return -1;
}

if (scalar(@{$r->{rows}}) == 0) {
    elog(ERROR, $r->{error});
    return -2;
}

#elog(WARNING, "AVAIL=" . ($r->{rows}->[0]->{available}) ? "TRUE" : "FALSE");
#elog(WARNING, Dumper $r->{rows});

if ($r->{rows}->[0]->{available} ne 't') {
    return 0;
}

spi_exec_query('BEGIN;');

my $q_selcond = <<EOQ;

    SELECT act.profile_parameter_id, act.change, act.is_delta, prf.value
    FROM mgdata.quests_onsuccess_actions_by_profile AS act
    LEFT JOIN mgdata.users_profiles AS prf
    ON prf.parameter_id = act.profile_parameter_id
    AND act.quest_id = '$qid' 
    AND prf.user_id = '$uid'
    
EOQ

my $rv_selcond = spi_exec_query($q_selcond);
if ($rv_selcond->{error}) {
    elog(ERROR, $rv_selcond->{error});
    spi_exec_query('ROLLBACK;');
    return -3;
}

my $q_apply = '';

for my $row (@{$rv_selcond->{rows}}) {
    if ($row->{is_delta}) {
        $q_apply .= <<EOC;

            UPDATE mgdata.users_profiles
            SET value = value::integer + $row->{change}
            WHERE parameter_id = $row->{profile_parameter_id}
            AND user_id = '$uid'
            ;

EOC
    } else {
        $q_apply .= <<EOC;

            UPDATE mgdata.users_profiles
            SET value = $row->{change}
            WHERE parameter_id = $row->{profile_parameter_id}
            AND user_id = '$uid'
            AND value::integer > $row->{change}  -- менять надо, если текущее значение больше того, на которое меняем
            -- иначе можно поменять в меньшую сторону
            ;

EOC
    }    

}

my $rv_apply = spi_exec_query($q_apply);
if ($rv_apply->{error}) {
    elog(ERROR, "QUERY: \n" . $q_apply . "\n\n" . $rv_apply->{error});
    spi_exec_query('ROLLBACK;');
    return -4;
}

my $q_start = <<EOQ;

    UPDATE mgdata.users_quests
    SET dateend = NOW(), success = TRUE
    WHERE user_id = '$uid'
    AND quest_id = '$qid'

EOQ

my $rv_start = spi_exec_query($q_start);
if ($rv_start->{error}) {
    elog(ERROR, $rv_start->{error});
    spi_exec_query('ROLLBACK;');
    return -5;
}

spi_exec_query('COMMIT;');


return 1;


$_$;
 ^   DROP FUNCTION mgdata.end_user_quest_with_success(OUT "OUT" integer, uid bigint, qid integer);
       mgdata       game    false    7    610            �            1255    25651    generate_new_user_map(bigint)    FUNCTION     �  CREATE FUNCTION generate_new_user_map(OUT result boolean, user_id bigint) RETURNS boolean
    LANGUAGE plperlu
    AS $_$

use strict;
use warnings;

use utf8;
use Encode;

use Mojo::JSON;

use Data::Dumper;

my $user_id = shift;
our $res = 1;

our ($dx, $dy) = (0, 0);

sub getSettings {
    my $q = <<EOQ_SETTINGS

SELECT mgdata.get_settings('user_begin_coord_x') AS dx, mgdata.get_settings('user_begin_coord_y') AS dy

EOQ_SETTINGS
;
    my $rv = spi_exec_query($q);
    if ($rv->{error}) {
        elog(ERROR, "QUERY: \n$q\nERROR: $@\n");
        return 0;
    }
    ($dx, $dy) = ($rv->{rows}->[0]->{dx}, $rv->{rows}->[0]->{dy});
    elog(INFO, "DX=$dx\nDY=$dy\n");
}

sub insertObject {
    my ($uid, $oid, $x, $y) = (@_);
    my $q = <<EOQ_INSERT
INSERT INTO mgdata.users_objects(user_id, object_id, coord_x, coord_y)
VALUES ('$uid', '$oid', '$x', '$y');
EOQ_INSERT
;
    my $rv = spi_exec_query($q);
    if ($rv->{error}) {
        elog(ERROR, "QUERY: \n$q\nERROR: $@\n");
        $res = 0;
        return 0;
    }

    return 1;
}

sub eraseUserMap {
    my $uid = shift; # user id
    #elog(INFO, "ERASING USER $uid MAP...");
    my $q = <<EOQ_ERASE
DELETE FROM mgdata.users_objects
WHERE user_id = '$uid';
EOQ_ERASE
;
    my $rv = spi_exec_query($q);
    if ($rv->{error}) {
        elog(ERROR, "QUERY: \n$q\nERROR: $@\n");
        $res = 0;
        return 0;
    }

    return 1;
}

sub fillUserMap {
    my ($uid, $oid, $dx, $dy, $x1, $y1, $x2, $y2) = @_;
    #elog(INFO, "FILL: " . Dumper @_);
    for (my $x=$x1; $x<=$x2; $x+=$dx) {
        for (my $y=$y1; $y<=$y2; $y+=$dy) {
            return 0 if !insertObject($uid, $oid, $x, $y);
        }
    }
    return 1;
}

spi_exec_query("BEGIN");

getSettings;
eraseUserMap($user_id);

#    0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35
################################################################################################################
#0 # 8  8  8  8  8  8  3  3  13 13 3  3  10 10 10 10 1  1  1  1  14 14 11 11 11 11 11 11 14 14 14 14 14 14 14 14
#1 # 8  8  8  8  8  8  3  3  13 13 3  3  10 10 10 10 1  1  1  1  14 14 11 11 11 11 11 11 14 14 14 14 14 14 14 14
#2 # 8  8  8  8  8  8  3  3  13 13 3  3  12 12 13 13 1  1  1  1  13 13 11 11 11 11 11 11 14 14 14 14 14 14 14 14
#3 # 8  8  8  8  8  8  3  3  13 13 3  3  12 12 13 13 1  1  1  1  13 13 11 11 11 11 11 11 14 14 14 14 14 14 14 14
#4 # 8  8  8  8  8  8  3  3  13 13 3  3  3  3  13 13 1  1  1  1  13 13 11 11 11 11 11 11 14 14 14 14 14 14 14 14
#5 # 8  8  8  8  8  8  3  3  13 13 3  3  3  3  13 13 1  1  1  1  13 13 11 11 11 11 11 11 14 14 14 14 14 14 14 14
#6 # 9  9  13 13 9  9  3  3  13 13 3  3  3  3  13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 14 14 14 14 14 14
#7 # 9  9  13 13 9  9  3  3  13 13 3  3  3  3  13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 14 14 14 14 14 14
#8 # 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 1  1  1  1  14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14
#9 # 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 1  1  1  1  14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14
#10# 7  7  13 13 6  6  3  3  3  3  3  3  3  3  3  3  1  1  1  1  14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14
#11# 7  7  13 13 6  6  3  3  3  3  3  3  3  3  3  3  1  1  1  1  14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14
#12# 7  7  13 13 6  6  3  3  3  3  3  3  3  3  3  3  1  1  1  1  14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14
#13# 7  7  13 13 6  6  3  3  3  3  3  3  3  3  3  3  1  1  1  1  14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14
#14# 3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  1  1  1  1  14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14
#15# 3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  3  1  1  1  1  14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14

# 1;"* unavailable";"* недоступно";1;1
return 0 if !fillUserMap($user_id, '1', 1,1, $dx+16, $dy, $dx+19, $dy+5);
return 0 if !fillUserMap($user_id, '1', 1,1, $dx+16, $dy+8, $dx+19, $dy+15);

# 3;"* used";"* куплено";1;1
return 0 if !fillUserMap($user_id, '3', 1,1, $dx+6, $dy, $dx+7, $dy+7);
return 0 if !fillUserMap($user_id, '3', 1,1, $dx+10, $dy, $dx+11, $dy+7);
return 0 if !fillUserMap($user_id, '3', 1,1, $dx+12, $dy+4, $dx+13, $dy+7);
return 0 if !fillUserMap($user_id, '3', 1,1, $dx, $dy+14, $dx+3, $dy+15);
return 0 if !fillUserMap($user_id, '3', 1,1, $dx+6, $dy+10, $dx+15, $dy+15);

# 6;"cottage";"Коттедж";2;2
return 0 if !insertObject($user_id, '6', $dx+4, $dy+10);
return 0 if !insertObject($user_id, '6', $dx+4, $dy+12);

# 7;"courtyard house";"Флигель";2;2
return 0 if !insertObject($user_id, '7', $dx, $dy+10);
return 0 if !insertObject($user_id, '7', $dx, $dy+12);

# 8;"mayoralty";"Мэрия";6;6
return 0 if !insertObject($user_id, '8', $dx+0, $dy+0);

# 9;"tree";"Дерево";2;2
return 0 if !insertObject($user_id, '9', $dx, $dy+6);
return 0 if !insertObject($user_id, '9', $dx+4, $dy+6);

# 10;"powerful wind generator";"Мощный ветряк";2;2
return 0 if !insertObject($user_id, '10', $dx+12, $dy);
return 0 if !insertObject($user_id, '10', $dx+14, $dy);

# 11;"terminal";"Тарминал";6;6
return 0 if !insertObject($user_id, '11', $dx+22, $dy);

# 12;"small lake";"Небольшое озеро";2;2
return 0 if !insertObject($user_id, '12', $dx+12, $dy+2);

# 13;"road";"Дорога";2;2
return 0 if !insertObject($user_id, '13', $dx+8, $dy);
return 0 if !insertObject($user_id, '13', $dx+8, $dy+2);
return 0 if !insertObject($user_id, '13', $dx+14, $dy+2);
return 0 if !insertObject($user_id, '13', $dx+8, $dy+4);
return 0 if !insertObject($user_id, '13', $dx+14, $dy+4);
return 0 if !insertObject($user_id, '13', $dx+2, $dy+6);
return 0 if !insertObject($user_id, '13', $dx+8, $dy+6);
return 0 if !insertObject($user_id, '13', $dx+14, $dy+6);
return 0 if !insertObject($user_id, '13', $dx+16, $dy+6);
return 0 if !insertObject($user_id, '13', $dx+18, $dy+6);
return 0 if !insertObject($user_id, '13', $dx+20, $dy+6);
return 0 if !insertObject($user_id, '13', $dx+22, $dy+6);
return 0 if !insertObject($user_id, '13', $dx+24, $dy+6);
return 0 if !insertObject($user_id, '13', $dx+26, $dy+6);
return 0 if !insertObject($user_id, '13', $dx+28, $dy+6);
return 0 if !insertObject($user_id, '13', $dx, $dy+8);
return 0 if !insertObject($user_id, '13', $dx+2, $dy+8);
return 0 if !insertObject($user_id, '13', $dx+4, $dy+8);
return 0 if !insertObject($user_id, '13', $dx+6, $dy+8);
return 0 if !insertObject($user_id, '13', $dx+8, $dy+8);
return 0 if !insertObject($user_id, '13', $dx+10, $dy+8);
return 0 if !insertObject($user_id, '13', $dx+12, $dy+8);
return 0 if !insertObject($user_id, '13', $dx+14, $dy+8);
return 0 if !insertObject($user_id, '13', $dx+2, $dy+10);
return 0 if !insertObject($user_id, '13', $dx+2, $dy+12);

# 14;"concrete";"Бетон";1;1
return 0 if !fillUserMap($user_id, '14', 1,1, $dx+28, $dy, $dx+35, $dy+5);
return 0 if !fillUserMap($user_id, '14', 1,1, $dx+30, $dy+6, $dx+35, $dy+7);
return 0 if !fillUserMap($user_id, '14', 1,1, $dx+20, $dy+8, $dx+35, $dy+15);

spi_exec_query("END;");

return 1;

$_$;
 P   DROP FUNCTION mgdata.generate_new_user_map(OUT result boolean, user_id bigint);
       mgdata       game    false    610    7            �            1255    25716 !   generate_new_user_profile(bigint)    FUNCTION     �  CREATE FUNCTION generate_new_user_profile("IN" bigint, OUT "OUT" boolean) RETURNS boolean
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;

use Mojo::JSON;

my $uid = shift; #user id

my $res = '';

spi_exec_query("BEGIN");

my $q = <<EOQ

DELETE FROM mgdata.users_profiles
WHERE user_id = '$uid';

SELECT "id", "default"
FROM mgdata.users_profiles_parameters

EOQ
;

my $rv = spi_exec_query($q);
if ($rv->{error}) {
    elog(ERROR, "QUERY:\n$q\nERROR:\n$@\n");
    return 0;
}

for my $row (@{$rv->{rows}}) {
    my $f_id = ${row}->{id};
    my $f_default = ${row}->{default};
    my $qins = <<EOQ_INSERT

INSERT INTO mgdata.users_profiles(user_id, parameter_id, value)
VALUES('$uid', '$f_id', '$f_default');

EOQ_INSERT
;
    my $rvins = spi_exec_query($qins);
    if ($rvins->{error}) {
        elog(ERROR, "QUERY:\n$qins\nERROR:\n$@\n");
        return 0;
    }
}

spi_exec_query("END;");

return 1;


$_$;
 P   DROP FUNCTION mgdata.generate_new_user_profile("IN" bigint, OUT "OUT" boolean);
       mgdata       game    false    610    7            �            1255    25671    get_objects_catalog()    FUNCTION     I  CREATE FUNCTION get_objects_catalog(OUT "OUT" character varying) RETURNS character varying
    LANGUAGE plperlu
    AS $_$

use strict;
use warnings;

use utf8;
use Encode;

use Mojo::JSON;

use Data::Dumper;

my $res = {
    objects => []
};

my $q = "SELECT * FROM mgdata.objects_catalog";
my $rv = spi_exec_query($q);
if ($rv->{error}) {
    elog(ERROR, "QUERY: $q\nERROR: $@\n");
    return '{"objects":"error"}';
}

for my $row (@{$rv->{rows}}) {
    #elog(INFO, "" . $row->{id} . "name_ru is UTF8: " . utf8::is_utf8($row->{name_ru}));
    #Encode::_utf8_on($row->{name_ru});
    #elog(INFO, "" . $row->{id} . "name_ru is UTF8: " . utf8::is_utf8($row->{name_ru}));
    #elog(INFO, "DUMPER: " . Dumper Encode::encode('utf8',$row->{name_ru}));
    push $res->{objects}, 
    {
        id => $row->{id},
        name => $row->{name},
        name_ru => $row->{name_ru},
        area_x => $row->{area_x},
        area_y => $row->{area_y},
        available_level => $row->{available_level}
    };

    #return $row->{name_ru};
}

return Encode::decode('utf8',Mojo::JSON->new->encode($res));
$_$;
 G   DROP FUNCTION mgdata.get_objects_catalog(OUT "OUT" character varying);
       mgdata       game    false    7    610            �            1255    25664    get_settings(character varying)    FUNCTION     �  CREATE FUNCTION get_settings(OUT "out" character varying, "in" character varying) RETURNS character varying
    LANGUAGE plperl
    AS $_$

my $name = shift;

my $q = <<EOQ

SELECT value FROM mgdata.settings
WHERE name = '$name';

EOQ
;

my $rv = spi_exec_query($q);
do {
    elog(ERROR, "QUERY: $q\nERROR: $@\n");
    return "ERROR";
} if $rv->{error};


return $rv->{rows}->[0]->{value};

$_$;
 X   DROP FUNCTION mgdata.get_settings(OUT "out" character varying, "in" character varying);
       mgdata       game    false    7    609            �            1255    25728    get_user_objects(bigint)    FUNCTION     �  CREATE FUNCTION get_user_objects(OUT "OUT" character varying, "IN" bigint) RETURNS character varying
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;

use Mojo::JSON;

use Data::Dumper;

my $uid = shift;
my $res = {
    usermap => []
};
my $qsel = <<EOQSEL

SELECT *
FROM mgdata.users_objects
WHERE user_id = '$uid';

EOQSEL
;

my $rvsel = spi_exec_query($qsel);
if ($rvsel->{error}) {
    elog(ERROR, "Query:\n$qsel\nERROR:\n$@\n" . $rvsel->{error});
    return '{"error":"1"}';
}


for my $row (@{$rvsel->{rows}}) {
    push @{$res->{usermap}}, {
        id => $row->{id},
        object_id => $row->{object_id},
        coord_x => $row->{coord_x},
        coord_y => $row->{coord_y}
    };
}


elog(INFO, "" . Dumper $res);

my $json = Mojo::JSON->new;
my $ret = $json->encode($res);
if ($json->error) {
    elog(ERROR, "WRONG JSON! " . $json->error);
    return '{"error":"1"}';
}

elog(INFO, "Length: " . length($ret));

return $ret;





$_$;
 Q   DROP FUNCTION mgdata.get_user_objects(OUT "OUT" character varying, "IN" bigint);
       mgdata       game    false    7    610            �            1255    25714    get_user_profile(bigint)    FUNCTION     �  CREATE FUNCTION get_user_profile(OUT "OUT" character varying, "INT" bigint) RETURNS character varying
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;

use utf8;
use Encode 'decode';

use Mojo::JSON;

my $uid = shift; # user id
elog(INFO, "UID: $uid\n");

my $res = {};

#my $q = <<EOQ
#
#SELECT param.id AS id, param.name AS name, userdata.value AS value
#FROM mgdata.users_profiles AS userdata
#LEFT JOIN mgdata.users_profiles_parameters AS param
#ON userdata.user_id = '$uid'
# AND userdata.parameter_id = param.id;
#
#EOQ
my $q = <<EOQ

SELECT param.id AS id, param.name AS name, userdata.value AS value
FROM mgdata.users_profiles AS userdata
LEFT JOIN mgdata.users_profiles_parameters AS param
ON 
 userdata.parameter_id = param.id
WHERE
 userdata.user_id = '19'

EOQ
;

my $rv = spi_exec_query($q);
if ($rv->{error}) {
    elog(INFO, "QUERY:\n$q\nERROR:\n$@\n");
    return '{"error":"1"}';
}

for my $row (@{$rv->{rows}}) {
#    $res->{$row->{name}} = {
#        id => $row->{id},
#        value => $row->{value}
#    };
    $res->{$row->{id}} = {
        name => $row->{name},
        value => $row->{value}
    };
}

return decode('utf8', Mojo::JSON->new->encode($res));


$_$;
 R   DROP FUNCTION mgdata.get_user_profile(OUT "OUT" character varying, "INT" bigint);
       mgdata       game    false    610    7            �            1255    33830 "   get_user_quests_available(integer)    FUNCTION     '  CREATE FUNCTION get_user_quests_available(OUT "OUT" character varying, "IN" integer) RETURNS character varying
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;

use utf8;
use Encode 'decode';

use Mojo::JSON;

my $uid = shift;
my $json = Mojo::JSON->new;

my $rv = spi_exec_query(<<EOQ

SELECT * FROM mgdata.users_quests_available
WHERE user_id = '$uid';

EOQ
);

if ($rv->{error}) {
    elog(ERROR, $rv->{error});
    return '{"error":"1"}';
}

my $res = {};
for my $row (@{$rv->{rows}}) {
    $res->{$row->{quest_id}} = {
        text => $row->{text},
        text_ru => $row->{text_ru},
        duration => $row->{duration}
    };
}


my $rjson = $json->encode($res);
if ($json->error) {
    elog(ERROR, $json->error);
    return '{"error":"1"}';
}

return decode('utf8', $rjson);



    $_$;
 [   DROP FUNCTION mgdata.get_user_quests_available(OUT "OUT" character varying, "IN" integer);
       mgdata       game    false    7    610            �            1255    33847    get_user_quests_ended(integer)    FUNCTION       CREATE FUNCTION get_user_quests_ended(OUT "OUT" character varying, "IN" integer) RETURNS character varying
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;

use utf8;
use Encode 'decode';

use Mojo::JSON;

my $uid = shift;
my $json = Mojo::JSON->new;

my $rv = spi_exec_query(<<EOQ

SELECT * FROM mgdata.users_quests_ended
WHERE user_id = '$uid';

EOQ
);

if ($rv->{error}) {
    elog(ERROR, $rv->{error});
    return '{"error":"1"}';
}

my $res = {};
for my $row (@{$rv->{rows}}) {
    $res->{$row->{quest_id}} = {
        text => $row->{text},
        text_ru => $row->{text_ru},
        duration => $row->{duration}
    };
}


my $rjson = $json->encode($res);
if ($json->error) {
    elog(ERROR, $json->error);
    return '{"error":"1"}';
}

return decode('utf8', $rjson);



    $_$;
 W   DROP FUNCTION mgdata.get_user_quests_ended(OUT "OUT" character varying, "IN" integer);
       mgdata       game    false    7    610            �            1255    33846     get_user_quests_started(integer)    FUNCTION     #  CREATE FUNCTION get_user_quests_started(OUT "OUT" character varying, "IN" integer) RETURNS character varying
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;

use utf8;
use Encode 'decode';

use Mojo::JSON;

my $uid = shift;
my $json = Mojo::JSON->new;

my $rv = spi_exec_query(<<EOQ

SELECT * FROM mgdata.users_quests_started
WHERE user_id = '$uid';

EOQ
);

if ($rv->{error}) {
    elog(ERROR, $rv->{error});
    return '{"error":"1"}';
}

my $res = {};
for my $row (@{$rv->{rows}}) {
    $res->{$row->{quest_id}} = {
        text => $row->{text},
        text_ru => $row->{text_ru},
        duration => $row->{duration}
    };
}


my $rjson = $json->encode($res);
if ($json->error) {
    elog(ERROR, $json->error);
    return '{"error":"1"}';
}

return decode('utf8', $rjson);



    $_$;
 Y   DROP FUNCTION mgdata.get_user_quests_started(OUT "OUT" character varying, "IN" integer);
       mgdata       game    false    7    610            �            1255    42004 !   start_user_quest(bigint, integer)    FUNCTION     '	  CREATE FUNCTION start_user_quest(OUT "OUT" integer, uid bigint, qid integer) RETURNS integer
    LANGUAGE plperlu
    AS $_$


use strict;
use warnings;

my ($uid, $qid) = @_;

my $q = <<EOQ;

SELECT * FROM mgdata.users_quests_available
WHERE user_id = '$uid' AND quest_id = '$qid'

EOQ

my $r = spi_exec_query($q);
if ($r->{error}) {
    elog(ERROR, $r->{error});
    return -1;
}

if (scalar(@{$r->{rows}}) == 0) {
    elog(WARNING, "Quest is not available!");
    return -2;
}


spi_exec_query('BEGIN;');

my $q_selcond = <<EOQ;

    SELECT act.profile_parameter_id, act.change, act.is_delta, prf.value
    FROM mgdata.quests_onstart_actions_by_profile AS act
    LEFT JOIN mgdata.users_profiles AS prf
    ON prf.parameter_id = act.profile_parameter_id
    AND act.quest_id = '$qid' 
    AND prf.user_id = '$uid'
    
EOQ

my $rv_selcond = spi_exec_query($q_selcond);
if ($rv_selcond->{error}) {
    elog(ERROR, $rv_selcond->{error});
    spi_exec_query('ROLLBACK;');
    return -3;
}

my $q_apply = '';

for my $row (@{$rv_selcond->{rows}}) {
    if ($row->{is_delta}) {
        $q_apply .= <<EOC;

            UPDATE mgdata.users_profiles
            SET value = value::integer + $row->{change}
            WHERE parameter_id = $row->{profile_parameter_id}
            AND user_id = '$uid'
            ;

EOC
    } else {
        $q_apply .= <<EOC;

            UPDATE mgdata.users_profiles
            SET value = $row->{change}
            WHERE parameter_id = $row->{profile_parameter_id}
            AND user_id = '$uid'
            AND value::integer > $row->{change}  -- менять надо, если текущее значение больше того, на которое меняем
            -- иначе можно поменять в меньшую сторону
            ;

EOC
    }    

}

my $rv_apply = spi_exec_query($q_apply);
if ($rv_apply->{error}) {
    elog(ERROR, "QUERY: \n" . $q_apply . "\n\n" . $rv_apply->{error});
    spi_exec_query('ROLLBACK;');
    return -4;
}

my $q_start = <<EOQ;

    INSERT INTO mgdata.users_quests(user_id, quest_id, datebegin)
    VALUES ('$uid', '$qid', NOW());

EOQ

my $rv_start = spi_exec_query($q_start);
if ($rv_start->{error}) {
    elog(ERROR, $rv_start->{error});
    spi_exec_query('ROLLBACK;');
    return -5;
}

spi_exec_query('COMMIT;');


return 1;


$_$;
 S   DROP FUNCTION mgdata.start_user_quest(OUT "OUT" integer, uid bigint, qid integer);
       mgdata       game    false    610    7            �            1259    17471    log_users_actions    TABLE       CREATE TABLE log_users_actions (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    user_app_id bigint NOT NULL,
    date timestamp without time zone DEFAULT now() NOT NULL,
    data_json character varying NOT NULL,
    user_date timestamp without time zone NOT NULL
);
 %   DROP TABLE mgdata.log_users_actions;
       mgdata         game    false    2559    7            �            1259    17469    log_users_actions_id_seq    SEQUENCE     z   CREATE SEQUENCE log_users_actions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE mgdata.log_users_actions_id_seq;
       mgdata       game    false    167    7            |
           0    0    log_users_actions_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE log_users_actions_id_seq OWNED BY log_users_actions.id;
            mgdata       game    false    166            �            1259    17495    objects_catalog    TABLE     )  CREATE TABLE objects_catalog (
    id bigint NOT NULL,
    name character varying NOT NULL,
    name_ru character varying,
    area_x integer DEFAULT 1 NOT NULL,
    area_y integer DEFAULT 1 NOT NULL,
    available_level integer DEFAULT 0 NOT NULL,
    is_static boolean DEFAULT false NOT NULL
);
 #   DROP TABLE mgdata.objects_catalog;
       mgdata         game    false    2561    2562    2563    2564    7            �            1259    17493    objects_catalog_id_seq    SEQUENCE     x   CREATE SEQUENCE objects_catalog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE mgdata.objects_catalog_id_seq;
       mgdata       game    false    7    169            }
           0    0    objects_catalog_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE objects_catalog_id_seq OWNED BY objects_catalog.id;
            mgdata       game    false    168            �            1259    25731    quests_catalog    TABLE     �   CREATE TABLE quests_catalog (
    id integer NOT NULL,
    text character varying NOT NULL,
    text_ru character varying,
    duration smallint DEFAULT 1 NOT NULL
);
 "   DROP TABLE mgdata.quests_catalog;
       mgdata         game    false    2574    7            ~
           0    0    COLUMN quests_catalog.duration    COMMENT     ]   COMMENT ON COLUMN quests_catalog.duration IS 'длительность в секундах';
            mgdata       game    false    179            �            1259    25729    quests_catalog_id_seq    SEQUENCE     w   CREATE SEQUENCE quests_catalog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE mgdata.quests_catalog_id_seq;
       mgdata       game    false    179    7            
           0    0    quests_catalog_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE quests_catalog_id_seq OWNED BY quests_catalog.id;
            mgdata       game    false    178            �            1259    25744    quests_conditions_by_profile    TABLE     �   CREATE TABLE quests_conditions_by_profile (
    id bigint NOT NULL,
    quest_id integer NOT NULL,
    profile_parameter_id smallint NOT NULL,
    value integer DEFAULT 1 NOT NULL
);
 0   DROP TABLE mgdata.quests_conditions_by_profile;
       mgdata         game    false    2576    7            �
           0    0 "   TABLE quests_conditions_by_profile    COMMENT     i   COMMENT ON TABLE quests_conditions_by_profile IS 'условия доступности квестов';
            mgdata       game    false    181            �            1259    25742 #   quests_conditions_by_profile_id_seq    SEQUENCE     �   CREATE SEQUENCE quests_conditions_by_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE mgdata.quests_conditions_by_profile_id_seq;
       mgdata       game    false    7    181            �
           0    0 #   quests_conditions_by_profile_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE quests_conditions_by_profile_id_seq OWNED BY quests_conditions_by_profile.id;
            mgdata       game    false    180            �            1259    25808     quests_onfail_actions_by_profile    TABLE     �   CREATE TABLE quests_onfail_actions_by_profile (
    id bigint NOT NULL,
    quest_id integer NOT NULL,
    profile_parameter_id smallint NOT NULL,
    change integer DEFAULT 1 NOT NULL,
    is_delta boolean DEFAULT false NOT NULL
);
 4   DROP TABLE mgdata.quests_onfail_actions_by_profile;
       mgdata         game    false    2584    2585    7            �
           0    0 &   TABLE quests_onfail_actions_by_profile    COMMENT     �   COMMENT ON TABLE quests_onfail_actions_by_profile IS 'действия при провале выполнения квеста';
            mgdata       game    false    187            �            1259    25806 '   quests_onfail_actions_by_profile_id_seq    SEQUENCE     �   CREATE SEQUENCE quests_onfail_actions_by_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE mgdata.quests_onfail_actions_by_profile_id_seq;
       mgdata       game    false    7    187            �
           0    0 '   quests_onfail_actions_by_profile_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE quests_onfail_actions_by_profile_id_seq OWNED BY quests_onfail_actions_by_profile.id;
            mgdata       game    false    186            �            1259    25770 !   quests_onstart_actions_by_profile    TABLE     �   CREATE TABLE quests_onstart_actions_by_profile (
    id bigint NOT NULL,
    quest_id integer NOT NULL,
    profile_parameter_id smallint NOT NULL,
    change integer DEFAULT 1 NOT NULL,
    is_delta boolean DEFAULT false NOT NULL
);
 5   DROP TABLE mgdata.quests_onstart_actions_by_profile;
       mgdata         game    false    2578    2579    7            �
           0    0 '   TABLE quests_onstart_actions_by_profile    COMMENT     �   COMMENT ON TABLE quests_onstart_actions_by_profile IS 'действия при начале выполнения квеста';
            mgdata       game    false    183            �            1259    25768 (   quests_onstart_actions_by_profile_id_seq    SEQUENCE     �   CREATE SEQUENCE quests_onstart_actions_by_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE mgdata.quests_onstart_actions_by_profile_id_seq;
       mgdata       game    false    183    7            �
           0    0 (   quests_onstart_actions_by_profile_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE quests_onstart_actions_by_profile_id_seq OWNED BY quests_onstart_actions_by_profile.id;
            mgdata       game    false    182            �            1259    25789 #   quests_onsuccess_actions_by_profile    TABLE     �   CREATE TABLE quests_onsuccess_actions_by_profile (
    id bigint NOT NULL,
    quest_id integer NOT NULL,
    profile_parameter_id smallint NOT NULL,
    change integer DEFAULT 1 NOT NULL,
    is_delta boolean DEFAULT false NOT NULL
);
 7   DROP TABLE mgdata.quests_onsuccess_actions_by_profile;
       mgdata         game    false    2581    2582    7            �
           0    0 )   TABLE quests_onsuccess_actions_by_profile    COMMENT     �   COMMENT ON TABLE quests_onsuccess_actions_by_profile IS 'действия при успешном выполнении квеста';
            mgdata       game    false    185            �            1259    25787 *   quests_onsuccess_actions_by_profile_id_seq    SEQUENCE     �   CREATE SEQUENCE quests_onsuccess_actions_by_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE mgdata.quests_onsuccess_actions_by_profile_id_seq;
       mgdata       game    false    185    7            �
           0    0 *   quests_onsuccess_actions_by_profile_id_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE quests_onsuccess_actions_by_profile_id_seq OWNED BY quests_onsuccess_actions_by_profile.id;
            mgdata       game    false    184            �            1259    25654    settings    TABLE     t   CREATE TABLE settings (
    id bigint NOT NULL,
    name character varying NOT NULL,
    value character varying
);
    DROP TABLE mgdata.settings;
       mgdata         game    false    7            �            1259    25652    settings_id_seq    SEQUENCE     q   CREATE SEQUENCE settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE mgdata.settings_id_seq;
       mgdata       game    false    7    173            �
           0    0    settings_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE settings_id_seq OWNED BY settings.id;
            mgdata       game    false    172            �            1259    33853    settings_str    VIEW     �  CREATE VIEW settings_str AS
    SELECT (sets1.value)::integer AS map_x, (sets2.value)::integer AS map_y, (sets3.value)::integer AS user_begin_coord_x, (sets4.value)::integer AS user_begin_coord_y FROM (((settings sets1 LEFT JOIN settings sets2 ON (((sets2.name)::text = 'map_y'::text))) LEFT JOIN settings sets3 ON (((sets3.name)::text = 'user_begin_coord_x'::text))) LEFT JOIN settings sets4 ON (((sets4.name)::text = 'user_begin_coord_y'::text))) WHERE ((sets1.name)::text = 'map_x'::text);
    DROP VIEW mgdata.settings_str;
       mgdata       game    false    2555    7            �            1259    17422    users    TABLE     �   CREATE TABLE users (
    id bigint NOT NULL,
    login character varying NOT NULL,
    pass character varying NOT NULL,
    salt character varying NOT NULL
);
    DROP TABLE mgdata.users;
       mgdata         game    false    7            �            1259    17434 
   users_apps    TABLE     �   CREATE TABLE users_apps (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    app_name character varying,
    sid character varying,
    sid_creation_date timestamp without time zone,
    last_used timestamp without time zone
);
    DROP TABLE mgdata.users_apps;
       mgdata         game    false    7            �            1259    17432    users_apps_id_seq    SEQUENCE     s   CREATE SEQUENCE users_apps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE mgdata.users_apps_id_seq;
       mgdata       game    false    7    165            �
           0    0    users_apps_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE users_apps_id_seq OWNED BY users_apps.id;
            mgdata       game    false    164            �            1259    17420    users_id_seq    SEQUENCE     n   CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE mgdata.users_id_seq;
       mgdata       game    false    7    163            �
           0    0    users_id_seq    SEQUENCE OWNED BY     /   ALTER SEQUENCE users_id_seq OWNED BY users.id;
            mgdata       game    false    162            �            1259    25611    users_objects    TABLE     �   CREATE TABLE users_objects (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    object_id bigint NOT NULL,
    coord_x integer DEFAULT 0 NOT NULL,
    coord_y integer DEFAULT 0 NOT NULL
);
 !   DROP TABLE mgdata.users_objects;
       mgdata         game    false    2566    2567    7            �            1259    25609    users_objects_id_seq    SEQUENCE     v   CREATE SEQUENCE users_objects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE mgdata.users_objects_id_seq;
       mgdata       game    false    171    7            �
           0    0    users_objects_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE users_objects_id_seq OWNED BY users_objects.id;
            mgdata       game    false    170            �            1259    25677    users_profiles    TABLE     �   CREATE TABLE users_profiles (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    parameter_id smallint NOT NULL,
    value character varying DEFAULT '0'::character varying NOT NULL
);
 "   DROP TABLE mgdata.users_profiles;
       mgdata         game    false    2570    7            �            1259    25675    users_profiles_id_seq    SEQUENCE     w   CREATE SEQUENCE users_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE mgdata.users_profiles_id_seq;
       mgdata       game    false    7    175            �
           0    0    users_profiles_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE users_profiles_id_seq OWNED BY users_profiles.id;
            mgdata       game    false    174            �            1259    25694    users_profiles_parameters    TABLE     �   CREATE TABLE users_profiles_parameters (
    id integer NOT NULL,
    name character varying NOT NULL,
    "default" character varying DEFAULT '0'::character varying NOT NULL
);
 -   DROP TABLE mgdata.users_profiles_parameters;
       mgdata         game    false    2572    7            �            1259    25692     users_profiles_parameters_id_seq    SEQUENCE     �   CREATE SEQUENCE users_profiles_parameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE mgdata.users_profiles_parameters_id_seq;
       mgdata       game    false    177    7            �
           0    0     users_profiles_parameters_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE users_profiles_parameters_id_seq OWNED BY users_profiles_parameters.id;
            mgdata       game    false    176            �            1259    33804    users_quests    TABLE        CREATE TABLE users_quests (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    quest_id integer NOT NULL,
    datebegin timestamp without time zone DEFAULT now() NOT NULL,
    dateend timestamp without time zone,
    success boolean DEFAULT false
);
     DROP TABLE mgdata.users_quests;
       mgdata         game    false    2587    2588    7            �            1259    33825    users_quests_available    VIEW     �  CREATE VIEW users_quests_available AS
    SELECT available_quests.user_id, available_quests.quest_id, quests_catalog.text, quests_catalog.text_ru, quests_catalog.duration FROM ((SELECT DISTINCT profile.user_id, cond.quest_id FROM ((quests_conditions_by_profile cond LEFT JOIN users_profiles profile ON (((profile.parameter_id = cond.profile_parameter_id) AND ((profile.value)::integer >= cond.value)))) LEFT JOIN users_quests quests ON (((quests.quest_id = cond.quest_id) AND (quests.user_id = profile.user_id)))) WHERE (quests.datebegin IS NULL)) available_quests LEFT JOIN quests_catalog quests_catalog ON ((available_quests.quest_id = quests_catalog.id)));
 )   DROP VIEW mgdata.users_quests_available;
       mgdata       game    false    2552    7            �            1259    33836    users_quests_ended    VIEW     �  CREATE VIEW users_quests_ended AS
    SELECT quests.user_id, quests.quest_id, quests_catalog.text, quests_catalog.text_ru, quests_catalog.duration, quests.datebegin, quests.dateend, quests.success FROM (users_quests quests LEFT JOIN quests_catalog quests_catalog ON ((quests.quest_id = quests_catalog.id))) WHERE ((NOT (quests.datebegin IS NULL)) AND (NOT (quests.dateend IS NULL)));
 %   DROP VIEW mgdata.users_quests_ended;
       mgdata       game    false    2554    7            �            1259    33802    users_quests_id_seq    SEQUENCE     u   CREATE SEQUENCE users_quests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE mgdata.users_quests_id_seq;
       mgdata       game    false    7    189            �
           0    0    users_quests_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE users_quests_id_seq OWNED BY users_quests.id;
            mgdata       game    false    188            �            1259    33831    users_quests_started    VIEW     |  CREATE VIEW users_quests_started AS
    SELECT quests.user_id, quests.quest_id, quests_catalog.text, quests_catalog.text_ru, quests_catalog.duration, quests.datebegin, quests.dateend, quests.success FROM (users_quests quests LEFT JOIN quests_catalog quests_catalog ON ((quests.quest_id = quests_catalog.id))) WHERE ((NOT (quests.datebegin IS NULL)) AND (quests.dateend IS NULL));
 '   DROP VIEW mgdata.users_quests_started;
       mgdata       game    false    2553    7            �	           2604    17474    id    DEFAULT     n   ALTER TABLE ONLY log_users_actions ALTER COLUMN id SET DEFAULT nextval('log_users_actions_id_seq'::regclass);
 C   ALTER TABLE mgdata.log_users_actions ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    167    166    167             
           2604    17498    id    DEFAULT     j   ALTER TABLE ONLY objects_catalog ALTER COLUMN id SET DEFAULT nextval('objects_catalog_id_seq'::regclass);
 A   ALTER TABLE mgdata.objects_catalog ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    168    169    169            
           2604    25734    id    DEFAULT     h   ALTER TABLE ONLY quests_catalog ALTER COLUMN id SET DEFAULT nextval('quests_catalog_id_seq'::regclass);
 @   ALTER TABLE mgdata.quests_catalog ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    178    179    179            
           2604    25747    id    DEFAULT     �   ALTER TABLE ONLY quests_conditions_by_profile ALTER COLUMN id SET DEFAULT nextval('quests_conditions_by_profile_id_seq'::regclass);
 N   ALTER TABLE mgdata.quests_conditions_by_profile ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    180    181    181            
           2604    25811    id    DEFAULT     �   ALTER TABLE ONLY quests_onfail_actions_by_profile ALTER COLUMN id SET DEFAULT nextval('quests_onfail_actions_by_profile_id_seq'::regclass);
 R   ALTER TABLE mgdata.quests_onfail_actions_by_profile ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    186    187    187            
           2604    25773    id    DEFAULT     �   ALTER TABLE ONLY quests_onstart_actions_by_profile ALTER COLUMN id SET DEFAULT nextval('quests_onstart_actions_by_profile_id_seq'::regclass);
 S   ALTER TABLE mgdata.quests_onstart_actions_by_profile ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    183    182    183            
           2604    25792    id    DEFAULT     �   ALTER TABLE ONLY quests_onsuccess_actions_by_profile ALTER COLUMN id SET DEFAULT nextval('quests_onsuccess_actions_by_profile_id_seq'::regclass);
 U   ALTER TABLE mgdata.quests_onsuccess_actions_by_profile ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    185    184    185            
           2604    25657    id    DEFAULT     \   ALTER TABLE ONLY settings ALTER COLUMN id SET DEFAULT nextval('settings_id_seq'::regclass);
 :   ALTER TABLE mgdata.settings ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    173    172    173            �	           2604    17425    id    DEFAULT     V   ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);
 7   ALTER TABLE mgdata.users ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    162    163    163            �	           2604    17437    id    DEFAULT     `   ALTER TABLE ONLY users_apps ALTER COLUMN id SET DEFAULT nextval('users_apps_id_seq'::regclass);
 <   ALTER TABLE mgdata.users_apps ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    165    164    165            
           2604    25614    id    DEFAULT     f   ALTER TABLE ONLY users_objects ALTER COLUMN id SET DEFAULT nextval('users_objects_id_seq'::regclass);
 ?   ALTER TABLE mgdata.users_objects ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    170    171    171            	
           2604    25680    id    DEFAULT     h   ALTER TABLE ONLY users_profiles ALTER COLUMN id SET DEFAULT nextval('users_profiles_id_seq'::regclass);
 @   ALTER TABLE mgdata.users_profiles ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    175    174    175            
           2604    25697    id    DEFAULT     ~   ALTER TABLE ONLY users_profiles_parameters ALTER COLUMN id SET DEFAULT nextval('users_profiles_parameters_id_seq'::regclass);
 K   ALTER TABLE mgdata.users_profiles_parameters ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    176    177    177            
           2604    33807    id    DEFAULT     d   ALTER TABLE ONLY users_quests ALTER COLUMN id SET DEFAULT nextval('users_quests_id_seq'::regclass);
 >   ALTER TABLE mgdata.users_quests ALTER COLUMN id DROP DEFAULT;
       mgdata       game    false    188    189    189            [
          0    17471    log_users_actions 
   TABLE DATA               Z   COPY log_users_actions (id, user_id, user_app_id, date, data_json, user_date) FROM stdin;
    mgdata       game    false    167    2674   �7      �
           0    0    log_users_actions_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('log_users_actions_id_seq', 424, true);
            mgdata       game    false    166            ]
          0    17495    objects_catalog 
   TABLE DATA               a   COPY objects_catalog (id, name, name_ru, area_x, area_y, available_level, is_static) FROM stdin;
    mgdata       game    false    169    2674   SI      �
           0    0    objects_catalog_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('objects_catalog_id_seq', 2, true);
            mgdata       game    false    168            g
          0    25731    quests_catalog 
   TABLE DATA               >   COPY quests_catalog (id, text, text_ru, duration) FROM stdin;
    mgdata       game    false    179    2674   �J      �
           0    0    quests_catalog_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('quests_catalog_id_seq', 3, true);
            mgdata       game    false    178            i
          0    25744    quests_conditions_by_profile 
   TABLE DATA               Z   COPY quests_conditions_by_profile (id, quest_id, profile_parameter_id, value) FROM stdin;
    mgdata       game    false    181    2674   K      �
           0    0 #   quests_conditions_by_profile_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('quests_conditions_by_profile_id_seq', 3, true);
            mgdata       game    false    180            o
          0    25808     quests_onfail_actions_by_profile 
   TABLE DATA               i   COPY quests_onfail_actions_by_profile (id, quest_id, profile_parameter_id, change, is_delta) FROM stdin;
    mgdata       game    false    187    2674   BK      �
           0    0 '   quests_onfail_actions_by_profile_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('quests_onfail_actions_by_profile_id_seq', 1, true);
            mgdata       game    false    186            k
          0    25770 !   quests_onstart_actions_by_profile 
   TABLE DATA               j   COPY quests_onstart_actions_by_profile (id, quest_id, profile_parameter_id, change, is_delta) FROM stdin;
    mgdata       game    false    183    2674   gK      �
           0    0 (   quests_onstart_actions_by_profile_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('quests_onstart_actions_by_profile_id_seq', 1, true);
            mgdata       game    false    182            m
          0    25789 #   quests_onsuccess_actions_by_profile 
   TABLE DATA               l   COPY quests_onsuccess_actions_by_profile (id, quest_id, profile_parameter_id, change, is_delta) FROM stdin;
    mgdata       game    false    185    2674   �K      �
           0    0 *   quests_onsuccess_actions_by_profile_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('quests_onsuccess_actions_by_profile_id_seq', 1, true);
            mgdata       game    false    184            a
          0    25654    settings 
   TABLE DATA               ,   COPY settings (id, name, value) FROM stdin;
    mgdata       game    false    173    2674   �K      �
           0    0    settings_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('settings_id_seq', 4, true);
            mgdata       game    false    172            W
          0    17422    users 
   TABLE DATA               /   COPY users (id, login, pass, salt) FROM stdin;
    mgdata       game    false    163    2674   �K      Y
          0    17434 
   users_apps 
   TABLE DATA               W   COPY users_apps (id, user_id, app_name, sid, sid_creation_date, last_used) FROM stdin;
    mgdata       game    false    165    2674   FP      �
           0    0    users_apps_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('users_apps_id_seq', 37, true);
            mgdata       game    false    164            �
           0    0    users_id_seq    SEQUENCE SET     4   SELECT pg_catalog.setval('users_id_seq', 32, true);
            mgdata       game    false    162            _
          0    25611    users_objects 
   TABLE DATA               J   COPY users_objects (id, user_id, object_id, coord_x, coord_y) FROM stdin;
    mgdata       game    false    171    2674   U      �
           0    0    users_objects_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('users_objects_id_seq', 39580, true);
            mgdata       game    false    170            c
          0    25677    users_profiles 
   TABLE DATA               C   COPY users_profiles (id, user_id, parameter_id, value) FROM stdin;
    mgdata       game    false    175    2674   �      �
           0    0    users_profiles_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('users_profiles_id_seq', 425, true);
            mgdata       game    false    174            e
          0    25694    users_profiles_parameters 
   TABLE DATA               A   COPY users_profiles_parameters (id, name, "default") FROM stdin;
    mgdata       game    false    177    2674   ��      �
           0    0     users_profiles_parameters_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('users_profiles_parameters_id_seq', 5, true);
            mgdata       game    false    176            q
          0    33804    users_quests 
   TABLE DATA               S   COPY users_quests (id, user_id, quest_id, datebegin, dateend, success) FROM stdin;
    mgdata       game    false    189    2674   �      �
           0    0    users_quests_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('users_quests_id_seq', 26, true);
            mgdata       game    false    188            %
           2606    17479    log_users_actions_id_prix 
   CONSTRAINT     b   ALTER TABLE ONLY log_users_actions
    ADD CONSTRAINT log_users_actions_id_prix PRIMARY KEY (id);
 U   ALTER TABLE ONLY mgdata.log_users_actions DROP CONSTRAINT log_users_actions_id_prix;
       mgdata         game    false    167    167    2675            '
           2606    17505    objects_catalog_id_prix 
   CONSTRAINT     ^   ALTER TABLE ONLY objects_catalog
    ADD CONSTRAINT objects_catalog_id_prix PRIMARY KEY (id);
 Q   ALTER TABLE ONLY mgdata.objects_catalog DROP CONSTRAINT objects_catalog_id_prix;
       mgdata         game    false    169    169    2675            6
           2606    25740    quests_catalog_id_prix 
   CONSTRAINT     \   ALTER TABLE ONLY quests_catalog
    ADD CONSTRAINT quests_catalog_id_prix PRIMARY KEY (id);
 O   ALTER TABLE ONLY mgdata.quests_catalog DROP CONSTRAINT quests_catalog_id_prix;
       mgdata         game    false    179    179    2675            8
           2606    25762 $   quests_conditions_by_profile_id_prix 
   CONSTRAINT     x   ALTER TABLE ONLY quests_conditions_by_profile
    ADD CONSTRAINT quests_conditions_by_profile_id_prix PRIMARY KEY (id);
 k   ALTER TABLE ONLY mgdata.quests_conditions_by_profile DROP CONSTRAINT quests_conditions_by_profile_id_prix;
       mgdata         game    false    181    181    2675            A
           2606    25814 (   quests_onfail_actions_by_profile_id_prix 
   CONSTRAINT     �   ALTER TABLE ONLY quests_onfail_actions_by_profile
    ADD CONSTRAINT quests_onfail_actions_by_profile_id_prix PRIMARY KEY (id);
 s   ALTER TABLE ONLY mgdata.quests_onfail_actions_by_profile DROP CONSTRAINT quests_onfail_actions_by_profile_id_prix;
       mgdata         game    false    187    187    2675            ;
           2606    25776 )   quests_onstart_actions_by_profile_id_prix 
   CONSTRAINT     �   ALTER TABLE ONLY quests_onstart_actions_by_profile
    ADD CONSTRAINT quests_onstart_actions_by_profile_id_prix PRIMARY KEY (id);
 u   ALTER TABLE ONLY mgdata.quests_onstart_actions_by_profile DROP CONSTRAINT quests_onstart_actions_by_profile_id_prix;
       mgdata         game    false    183    183    2675            >
           2606    25795 +   quests_onsuccess_actions_by_profile_id_prix 
   CONSTRAINT     �   ALTER TABLE ONLY quests_onsuccess_actions_by_profile
    ADD CONSTRAINT quests_onsuccess_actions_by_profile_id_prix PRIMARY KEY (id);
 y   ALTER TABLE ONLY mgdata.quests_onsuccess_actions_by_profile DROP CONSTRAINT quests_onsuccess_actions_by_profile_id_prix;
       mgdata         game    false    185    185    2675            -
           2606    25662    settings_id_prix 
   CONSTRAINT     P   ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_id_prix PRIMARY KEY (id);
 C   ALTER TABLE ONLY mgdata.settings DROP CONSTRAINT settings_id_prix;
       mgdata         game    false    173    173    2675            !
           2606    17442    users_apps_id_prix 
   CONSTRAINT     T   ALTER TABLE ONLY users_apps
    ADD CONSTRAINT users_apps_id_prix PRIMARY KEY (id);
 G   ALTER TABLE ONLY mgdata.users_apps DROP CONSTRAINT users_apps_id_prix;
       mgdata         game    false    165    165    2675            
           2606    17430    users_id_prix 
   CONSTRAINT     J   ALTER TABLE ONLY users
    ADD CONSTRAINT users_id_prix PRIMARY KEY (id);
 =   ALTER TABLE ONLY mgdata.users DROP CONSTRAINT users_id_prix;
       mgdata         game    false    163    163    2675            +
           2606    25618    users_objects_id_prix 
   CONSTRAINT     Z   ALTER TABLE ONLY users_objects
    ADD CONSTRAINT users_objects_id_prix PRIMARY KEY (id);
 M   ALTER TABLE ONLY mgdata.users_objects DROP CONSTRAINT users_objects_id_prix;
       mgdata         game    false    171    171    2675            3
           2606    25703     users_profile_parameters_id_prix 
   CONSTRAINT     q   ALTER TABLE ONLY users_profiles_parameters
    ADD CONSTRAINT users_profile_parameters_id_prix PRIMARY KEY (id);
 d   ALTER TABLE ONLY mgdata.users_profiles_parameters DROP CONSTRAINT users_profile_parameters_id_prix;
       mgdata         game    false    177    177    2675            0
           2606    25686    users_profiles_id_prix 
   CONSTRAINT     \   ALTER TABLE ONLY users_profiles
    ADD CONSTRAINT users_profiles_id_prix PRIMARY KEY (id);
 O   ALTER TABLE ONLY mgdata.users_profiles DROP CONSTRAINT users_profiles_id_prix;
       mgdata         game    false    175    175    2675            D
           2606    33810    users_quests_id_prix 
   CONSTRAINT     X   ALTER TABLE ONLY users_quests
    ADD CONSTRAINT users_quests_id_prix PRIMARY KEY (id);
 K   ALTER TABLE ONLY mgdata.users_quests DROP CONSTRAINT users_quests_id_prix;
       mgdata         game    false    189    189    2675            )
           1259    25629    fki_users_objects_object_id_ext    INDEX     W   CREATE INDEX fki_users_objects_object_id_ext ON users_objects USING btree (object_id);
 3   DROP INDEX mgdata.fki_users_objects_object_id_ext;
       mgdata         game    false    171    2675            (
           1259    17506    objects_catalog_name_uniq    INDEX     U   CREATE UNIQUE INDEX objects_catalog_name_uniq ON objects_catalog USING btree (name);
 -   DROP INDEX mgdata.objects_catalog_name_uniq;
       mgdata         game    false    169    2675            9
           1259    25825 ?   quests_conditions_by_profile_quest_id_profile_parameter_id_uniq    INDEX     �   CREATE UNIQUE INDEX quests_conditions_by_profile_quest_id_profile_parameter_id_uniq ON quests_conditions_by_profile USING btree (quest_id, profile_parameter_id);
 S   DROP INDEX mgdata.quests_conditions_by_profile_quest_id_profile_parameter_id_uniq;
       mgdata         game    false    181    181    2675            B
           1259    25826 ?   quests_onfail_actions_by_profile_quest_id_profile_condition_id_    INDEX     �   CREATE UNIQUE INDEX quests_onfail_actions_by_profile_quest_id_profile_condition_id_ ON quests_onfail_actions_by_profile USING btree (quest_id, profile_parameter_id);
 S   DROP INDEX mgdata.quests_onfail_actions_by_profile_quest_id_profile_condition_id_;
       mgdata         game    false    187    187    2675            <
           1259    25827 ?   quests_onstart_actions_by_profile_quest_id_profile_condition_id    INDEX     �   CREATE UNIQUE INDEX quests_onstart_actions_by_profile_quest_id_profile_condition_id ON quests_onstart_actions_by_profile USING btree (quest_id, profile_parameter_id);
 S   DROP INDEX mgdata.quests_onstart_actions_by_profile_quest_id_profile_condition_id;
       mgdata         game    false    183    183    2675            ?
           1259    25828 ?   quests_onsuccess_actions_by_profile_quest_id_profile_condition_    INDEX     �   CREATE UNIQUE INDEX quests_onsuccess_actions_by_profile_quest_id_profile_condition_ ON quests_onsuccess_actions_by_profile USING btree (quest_id, profile_parameter_id);
 S   DROP INDEX mgdata.quests_onsuccess_actions_by_profile_quest_id_profile_condition_;
       mgdata         game    false    185    185    2675            .
           1259    25663    settings_name_uniq    INDEX     G   CREATE UNIQUE INDEX settings_name_uniq ON settings USING btree (name);
 &   DROP INDEX mgdata.settings_name_uniq;
       mgdata         game    false    173    2675            "
           1259    17491    users_apps_sid_uniq    INDEX     I   CREATE UNIQUE INDEX users_apps_sid_uniq ON users_apps USING btree (sid);
 '   DROP INDEX mgdata.users_apps_sid_uniq;
       mgdata         game    false    165    2675            #
           1259    17490     users_apps_user_id_app_name_uniq    INDEX     d   CREATE UNIQUE INDEX users_apps_user_id_app_name_uniq ON users_apps USING btree (user_id, app_name);
 4   DROP INDEX mgdata.users_apps_user_id_app_name_uniq;
       mgdata         game    false    165    165    2675            
           1259    17431    users_login_uniq    INDEX     C   CREATE UNIQUE INDEX users_login_uniq ON users USING btree (login);
 $   DROP INDEX mgdata.users_login_uniq;
       mgdata         game    false    163    2675            4
           1259    25704 #   users_profiles_parameters_name_uniq    INDEX     i   CREATE UNIQUE INDEX users_profiles_parameters_name_uniq ON users_profiles_parameters USING btree (name);
 7   DROP INDEX mgdata.users_profiles_parameters_name_uniq;
       mgdata         game    false    177    2675            1
           1259    25711 #   users_profiles_user_id_parameter_id    INDEX     o   CREATE UNIQUE INDEX users_profiles_user_id_parameter_id ON users_profiles USING btree (user_id, parameter_id);
 7   DROP INDEX mgdata.users_profiles_user_id_parameter_id;
       mgdata         game    false    175    175    2675            F
           2606    17480 !   log_users_actions_user_app_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY log_users_actions
    ADD CONSTRAINT log_users_actions_user_app_id_ext FOREIGN KEY (user_app_id) REFERENCES users_apps(id);
 ]   ALTER TABLE ONLY mgdata.log_users_actions DROP CONSTRAINT log_users_actions_user_app_id_ext;
       mgdata       game    false    2592    165    167    2675            G
           2606    17485    log_users_actions_users_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY log_users_actions
    ADD CONSTRAINT log_users_actions_users_id_ext FOREIGN KEY (user_id) REFERENCES users(id);
 Z   ALTER TABLE ONLY mgdata.log_users_actions DROP CONSTRAINT log_users_actions_users_id_ext;
       mgdata       game    false    2589    167    163    2675            L
           2606    25756 -   quests_conditions_by_profile_parameter_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY quests_conditions_by_profile
    ADD CONSTRAINT quests_conditions_by_profile_parameter_id_ext FOREIGN KEY (profile_parameter_id) REFERENCES users_profiles_parameters(id) MATCH FULL;
 t   ALTER TABLE ONLY mgdata.quests_conditions_by_profile DROP CONSTRAINT quests_conditions_by_profile_parameter_id_ext;
       mgdata       game    false    181    2610    177    2675            M
           2606    25763 )   quests_conditions_by_profile_quest_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY quests_conditions_by_profile
    ADD CONSTRAINT quests_conditions_by_profile_quest_id_ext FOREIGN KEY (quest_id) REFERENCES quests_catalog(id) MATCH FULL;
 p   ALTER TABLE ONLY mgdata.quests_conditions_by_profile DROP CONSTRAINT quests_conditions_by_profile_quest_id_ext;
       mgdata       game    false    181    2613    179    2675            R
           2606    25815 <   quests_onfail_actions_by_profile_by_profile_parameter_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY quests_onfail_actions_by_profile
    ADD CONSTRAINT quests_onfail_actions_by_profile_by_profile_parameter_id_ext FOREIGN KEY (profile_parameter_id) REFERENCES users_profiles_parameters(id) MATCH FULL;
 �   ALTER TABLE ONLY mgdata.quests_onfail_actions_by_profile DROP CONSTRAINT quests_onfail_actions_by_profile_by_profile_parameter_id_ext;
       mgdata       game    false    2610    187    177    2675            S
           2606    25820 -   quests_onfail_actions_by_profile_quest_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY quests_onfail_actions_by_profile
    ADD CONSTRAINT quests_onfail_actions_by_profile_quest_id_ext FOREIGN KEY (quest_id) REFERENCES quests_catalog(id) MATCH FULL ON DELETE CASCADE;
 x   ALTER TABLE ONLY mgdata.quests_onfail_actions_by_profile DROP CONSTRAINT quests_onfail_actions_by_profile_quest_id_ext;
       mgdata       game    false    187    2613    179    2675            N
           2606    25777 =   quests_onstart_actions_by_profile_by_profile_parameter_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY quests_onstart_actions_by_profile
    ADD CONSTRAINT quests_onstart_actions_by_profile_by_profile_parameter_id_ext FOREIGN KEY (profile_parameter_id) REFERENCES users_profiles_parameters(id) MATCH FULL;
 �   ALTER TABLE ONLY mgdata.quests_onstart_actions_by_profile DROP CONSTRAINT quests_onstart_actions_by_profile_by_profile_parameter_id_ext;
       mgdata       game    false    183    177    2610    2675            O
           2606    25782 .   quests_onstart_actions_by_profile_quest_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY quests_onstart_actions_by_profile
    ADD CONSTRAINT quests_onstart_actions_by_profile_quest_id_ext FOREIGN KEY (quest_id) REFERENCES quests_catalog(id) MATCH FULL ON DELETE CASCADE;
 z   ALTER TABLE ONLY mgdata.quests_onstart_actions_by_profile DROP CONSTRAINT quests_onstart_actions_by_profile_quest_id_ext;
       mgdata       game    false    183    179    2613    2675            P
           2606    25796 ?   quests_onsuccess_actions_by_profile_by_profile_parameter_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY quests_onsuccess_actions_by_profile
    ADD CONSTRAINT quests_onsuccess_actions_by_profile_by_profile_parameter_id_ext FOREIGN KEY (profile_parameter_id) REFERENCES users_profiles_parameters(id) MATCH FULL;
 �   ALTER TABLE ONLY mgdata.quests_onsuccess_actions_by_profile DROP CONSTRAINT quests_onsuccess_actions_by_profile_by_profile_parameter_id_ext;
       mgdata       game    false    2610    185    177    2675            Q
           2606    25801 0   quests_onsuccess_actions_by_profile_quest_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY quests_onsuccess_actions_by_profile
    ADD CONSTRAINT quests_onsuccess_actions_by_profile_quest_id_ext FOREIGN KEY (quest_id) REFERENCES quests_catalog(id) MATCH FULL ON DELETE CASCADE;
 ~   ALTER TABLE ONLY mgdata.quests_onsuccess_actions_by_profile DROP CONSTRAINT quests_onsuccess_actions_by_profile_quest_id_ext;
       mgdata       game    false    179    2613    185    2675            E
           2606    17443    users_apps_users_ext    FK CONSTRAINT     {   ALTER TABLE ONLY users_apps
    ADD CONSTRAINT users_apps_users_ext FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL;
 I   ALTER TABLE ONLY mgdata.users_apps DROP CONSTRAINT users_apps_users_ext;
       mgdata       game    false    163    2589    165    2675            I
           2606    25624    users_objects_object_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY users_objects
    ADD CONSTRAINT users_objects_object_id_ext FOREIGN KEY (object_id) REFERENCES objects_catalog(id) MATCH FULL;
 S   ALTER TABLE ONLY mgdata.users_objects DROP CONSTRAINT users_objects_object_id_ext;
       mgdata       game    false    169    171    2598    2675            H
           2606    25619    users_objects_user_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY users_objects
    ADD CONSTRAINT users_objects_user_id_ext FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL;
 Q   ALTER TABLE ONLY mgdata.users_objects DROP CONSTRAINT users_objects_user_id_ext;
       mgdata       game    false    2589    171    163    2675            K
           2606    25705    users_profiles_parameter_ext    FK CONSTRAINT     �   ALTER TABLE ONLY users_profiles
    ADD CONSTRAINT users_profiles_parameter_ext FOREIGN KEY (parameter_id) REFERENCES users_profiles_parameters(id) MATCH FULL;
 U   ALTER TABLE ONLY mgdata.users_profiles DROP CONSTRAINT users_profiles_parameter_ext;
       mgdata       game    false    177    2610    175    2675            J
           2606    25687    users_profiles_user_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY users_profiles
    ADD CONSTRAINT users_profiles_user_id_ext FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL;
 S   ALTER TABLE ONLY mgdata.users_profiles DROP CONSTRAINT users_profiles_user_id_ext;
       mgdata       game    false    2589    163    175    2675            U
           2606    33816    users_quests_quest_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY users_quests
    ADD CONSTRAINT users_quests_quest_id_ext FOREIGN KEY (quest_id) REFERENCES quests_catalog(id) MATCH FULL;
 P   ALTER TABLE ONLY mgdata.users_quests DROP CONSTRAINT users_quests_quest_id_ext;
       mgdata       game    false    2613    179    189    2675            T
           2606    33811    users_quests_user_id_ext    FK CONSTRAINT     �   ALTER TABLE ONLY users_quests
    ADD CONSTRAINT users_quests_user_id_ext FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL;
 O   ALTER TABLE ONLY mgdata.users_quests DROP CONSTRAINT users_quests_user_id_ext;
       mgdata       game    false    163    189    2589    2675            [
      x���ߎ�8����bu�wQ����c.�V��
����ݩ*;�ԯ\�iĹ�*�kO�>�I;�~Y�?��n�X�Y�=��ۗ�|�����_���a��.����y�_��ן�>��U�a����3]*7�T��%)�~O�^��]�=���R٣J#E�hX�A-D�������?����^ ��e�r����4RT��ePg�<�����7�z����.�oG�g��а����7�J��.s#M�h�ˀ:��<"x�Ǒ͍ʍ4��!�u��#xp�FRU)�DC,�L����IK��ˊ���6�Nvن#�(�F#�AMQS�wP'�l��/-���27�T����;�S]��~��u�˹"j�!!��d�m8ԯ����HS9ԯS��<�C����/n��	�שS]��~띜��7D�[HP�N��	�P��^���F�Zj�P�N����m���27�T����ԩ.��P�z���27�T����ԩ.��P?>]N`��h�P�N��G�e��5�4RT����;�3]�ҏRq��R#Q�~u��6�G����Q�
ҏS�r�]��e���[��Q�
��Nvن#�r���%�4�T����;�S]��~yb��F���P�~y~��W�_�����F��ѰB�:u��#8�o�����r4�P�m~\¡~�}�Gn��+ԯS��<�C�꽸7���r4�P�N�����+�=��~�HQ%V��A����!��:q�-�4��aC�ԩ.��H�B��qYi*GÆ�;�S]��~��ߥ]ߎF�G4lJ�'��e��k�<��h��y���:�e���Tjw��]N�[��Cæ�{�Nuy ߕ~�ʞ~G#M�hؕ~Oԩ.��J�e�/鞝.�Fj��]��D�����~��ko�;]�=v�߃�wفk�2&�ݢ���r4�Z?IŹ��E�x�֏�Kt���хڣa��=�~������~�~����r4T�߃:��\�W�)y7k�F�*�P�~-5u���I?�W{2w��\�7sd�Fo��c��|h���n?�ėPI�B�k���J��r#��h����VtSx�p��K����iW)�DC�C�L�G��F.n_b�d�;^���K���<��1��ܿ��.�7����7��{��i�;��<��-������ݡ.C�TǇ�U�)�,�{�>��H�%J�m�����Kn�l���(�6�D�FoG�g��P�!���_nR��%[⽘��07ZX�^����K[�߈��5;�*8����%p2�ޒu�����%F�O����χF�!x������/�I�#[y�k��pַ�|�����v.�on|��0t.���|���!\s;�Vs����!����@J�4p��b4}
�@ ��4m# ���'KR%J������rR9J�����`�ɤ�%`���V���/�!`1���WS�l���0�)���w�7bS8k��!��3�Ĕ�)e��%�I�(�)/ �)�5MS<�VO�4L�ۦV���%Q���k4L�z<�(S>D��2��~ؓ`��Ҳ�h� 32ex�F{��Ȕ��Ȕ�5&���@��22e���֬ד(�U.���Ҳ�hӓH�CF��D���1��$�h�S�MOj`[�dÔO�a��6����ܣ4L�<�0em�L�u^�Q��힔L�y�G	h�BY{<�$��US>4LY�/�&_�\VÔ�S�{����Q�l����W�
{�'DVÔ���	�a��p���)�h
�������{4L�`
�_��0eMC�a�s�.s�j�RG�a�l�v5L�l���aJm7�?4L�`Įr{3L����Ħ,7�Z�%��,QbS\�����¦p����@l�ԓ�D	�M�9��������������I���@�r4,;6e]I�����v%�cS�х��L��,f��|Ʒ#�|B@eʫ@eʙE��	LHe�+S^*S�,���!��F�rf�l~��/���s׷ɟ��~���?���_���OԢ��n��Kڛ������߂����9᪁KUZM�aj�*��,Z�N �Vs�iuZ�^��@PcI�~�Ъ��3S��GK��"T��v�����q����p��/�-����֞�����Q��!������.	>�>;X����$��% v��hj;�+r��/ ���E�_@��� p# ֊���n�$T?Zr}:v�l����y�����!�r]NN ��.p�@~�;�Yt994��z�(��/,3%!?�|�$�ד� �\%ѓ`�9ز����	���(��-k�A��� ^K�G	�����vܒhM?X� �t�`����j�%�k���w,��k�9ز�������\٣4���6hhՁ3%!�GK�̓z{C��m%�?o���q�9���:S�(wsj󀆃�c�Lmz{Ù�ǱPoox�)	n���e�&k�
���� tE���g����70�@tM4̟F�,&]M �!p\'>}�$|��~z���|'�J����i��I}W�Q�7N@$�	�!���X+�̏���@�k�Y�h�@���E+Qb�8��'0A ��
i�y�'܌'/I��Q"�^"SZ֨Þ�@�22�p��!��D�^��Ҳ�z=	��^A��D�H��{y�,�yN	��ǣM�zRz��Ȕ�5�L� ^G�% 2�eѕ�	�HW.�B�ܶ�c`�,��Ҳ�z���.�(�)��O�u��y���B�,�Ӿ�I�(�)/ S����	u��WÔ�� ���Dߓ��]��0e7'zh�������a5L��8'���]�����A��T@���Ys�7l�Գ�D	�M�98H�m����'E��7g6l�@l
g�gOB �7l
g�yyĦ��H�-��������x�߰5�CW<! ����(I�	k��|�ckJ��FN�.r�[�Ys�m��	:�ؠ��k���,W�;6����A.�P��wlP-�
\��ٱA��	�pà�^���@��0��@à��7��O�h��0���@� ���n�`>�!I��^bK��� {��H���H��5Ɛ��@Z_U$Q��+�$� ����_��?��\E"��Rc�L{R�X�D"��QU=	�\FH���D�l�G��$��% �e�2�9.P��D	��ᷡ�g9�ѭG	��q��l8J@lg��<bs\ (����0%��=������7��暸'5��4��4p1��暸'!��h1ı�Y��&�ɪ+-�G���wX��T�bx���t�U�K-�G�����/�9=!��ю�Jzz���<>�9J@�Q��[=��)b�8k�#IB ���=z�=�y�%	�tR"���Ii@�|���']��Q6��''��<z���#�Gc�{�Ys4�8��y֣�D	h�3���4
)�y|�=��wX@�ʏ�!�Y�9�ZL�{��SF�aNj�%J@,�\��^���6hYsm\�b� b����`������B@�gM1%	���@�
n hY��7���(q���C��I�d�L�-��~'TVhz��~���{X��UĄ�	^�ԑ��^�t��`�^EL���em�+4��G��Ú1{��ѐ����5��"�&���G	��iY��< g��yԲ����[@�Q�ME�]B�ۑ|<��,�/�$�|�v�,.KęF���,.�8:�>�|0��@�R����՚ӏ�V���惞���!x4�ʜ�6���V5�Q�Tk�=��I�|�#��!�LG{(�n֞�#��% ���x�����
�5$�����-��-���E�YS�tN����|�Qjcښв�O8=�"`�Eښ0� p# ���vY�X�������(q<`�8���3�v'���}��[@�#xݏrDCB�zvT�e��oh��GR�-�	mP�Y�J�@�#�T�s������>��3���6(��]��=k2 �  >G�oh��A�F��Φ��<�ifpN28�-Q�Xl>'5��!�-����w����h��%9�z&Q��5"O@�u�����@Z��-��V���	_!��oh��- �*�f�'��&h�c�g͡o�h��P��% �(�f�gd�{d�=��I?�X+�f��5�=k�$c ��.�@Y����7��|�=*�_�$P��q���@���DCFz����_wD	�=*����􃏵r���=*��;�=r��h/����*����o�G�5�V��U���|�r>Y	�<�nH����pf�h?T�w��Co`8�h�8����A ��j�o�GG6��n�<���R�A ���̢����M��M�      ]
   t  x�mR�N1]����]�_�T�<b�1�@�!l4bP������A��n���I�`�Mf��s�=�m�v��ʦ��Za�37t#��g�
TJhv�*�E�]r��8���'Fj�'~pWn�s7�
ʷ��!�4"����A~�`Í ���W �Q��4�)S�h�kƑ��Xiy�� ��?h)܄TDyr�L'���z�-ϼa7�ԩ��ϰ��<�k�H�E*TM��uE|������@�ؾ4�h$8�g��,�o�a�L"#�;��IH��,R�%�Z�@���΍�9���g���r�W��`���2^m�"�*�j�ٟr�l!;/77rH�(�=�~��k}���㭗�����:lp��G�w      g
   6   x�3�,I-.��ta��F ���& ުpa��� ��;.l�442������ �3�      i
      x�3�4�4�4������ ��      o
      x�3�4�4�4�=... �      k
      x�3�4�4�4�,����� 1      m
      x�3�4�4�4�L����� �      a
   :   x�3��M,���4400�2s*!c���Ԣ����̼������2.L	��=... ���      W
   7  x�M�iS�G�?�������ݳ��-D�h�����K�`D����V�����s��gh���/h
��*	s�N�ͨ%�H=8Jd��?X_��������WH�:q/���> 3�hℲ�z�����u3�7"�C�#A���"v9�X������t���i�A(1ǦR�S��=�Ҙ0I7{֮<}|t��l�i�	)E����@W�����w)��7o7�-~Zxvj�L�L%��%�Q��:rwM-p��ѼY_]ߍ�_�i�`�[T�z��+�RR�Гgl�|�`>o���BvoY�)����k���`�FW2u2�=�k7�m�B3J D򐢰ɑ;�J0����K�a�Џ��R�)��A3I�d��L[>����"N��F�#'9��>�PFMN�o�\|3�ʥ��-N��(��Og�������;�:ĥd��ܯ'���Z����D�K=��D?0��Z��ٛ��͛{����)z��gn�Ř3��i��U}�݄k�o��^ز8e�#���C�k�q�jrsQ�G���_��riMnY����]�Mc��b� �F(gF��Wє��RK�����F>R&�5E.��>ڱ8�C#i��D蕼�!�k��H���99ڙ�M�-�D �Q}��[tY_E1�<b
�1k����:�1)�dHj���v%�Q�T%:�����׏6,M��B��1K�\=��)w�HѼ��x5��'-M$�3��׺sQ��tPɞ<t��=��qHnX�H oHo��@dQd��
U��6�R��WG�w�����CNK&�\�qK�0U�`$�������ޙ��D��BR�QW*��:b���0U��9���mm�&K��h|I�r��,)	~5=`iZB��#����oz��v��9"��j5��Y�U)*����� p`� G-_�ޚ���U`"Izj5���{�8���di���a�F�Z�S��Q[t(w���2����Ž����?�M,82C�@�8�������R��T��lr=��o��[7��i1�?���}���C��N�Y���<����տ_}��g3 ������M��$J�$=�F��ћ�s[W���1o_�Xk�;��      Y
   �  x�mVK�,�\K��<�_����፾�c��?��՝���T4�"� EGJ�Rc��6y�s�Z�W;:��/�_$_\^�/���X����O鲶H�Ke�lsT�������	��S[g��V�Y#����ڲ^o��/y�"K��SC~\��m���k��QeY����C����^�OX���i�hk�x�T"E��w�a�T���l����_FzAD>�Z(o�%oն�Q�H�_�OX��xn��pw�Pj?n�-j��N�P��s�[2<gg-��b��M�:�9\��׏�����.,C*���\��ꥹ�㬦��C��W���e�s�M]hD�Ҡ���m�z[�G��C��?���������c�ɰ«��Y6S4�3~fg����',3%��7/L�����t|qG�:֏F��@e��e�K�{,<��=����x�qz�ZV�I��?a��I	E�Wq2��6Z��5*�U[��}�,�����D��e�������^ ;������	�d�;5�,�Pi/g�������6n�G*^��|q��?�_�g%�I�}�y��7���fS\V-�~��8^���',C灚�6j�WnS�����]�L��r�O铓�XfK�r�Z'׺z+1�P�	xH����+/�',_��0ӳ����@"UJ�({ϊ�ˀ%��ty7���6d�2�#����<fWǩ���iޓ��0�,_�|��yzU6�ވ�m�=�'�=�=����,_D���q&��7R[���Y�^x�b��G.,s��A:1F��k�skbeK����m$��;)/�',C�|9ͶR~�i�60�`�[v��n�h����}!���x}����\#�vx�2j�ǖ�f~`�ZT��
y�H�����Y�o����{ �����!�F�ƥ.��l��S��'�=�T���e�_�&�v��P�9A�g����f�A��0����Mɴm��~���[Ԃ�7x��{�>���
a"��}~���66I��B�	��L��-���~�2�V�t/�TtXKqH�L�`]b�Ml\������x�2��'mU8��i�j'��Ԫ��n�G.��V,��/�pPb��z�3J������	�/�ߴ�.mh�8��XV���-fgp ���vh��mJ��$���� �t*��j|�*���"�Bv�l��=?��G�п�@1���~�y�u]�#�7������p����s��ꁡ      _
      x�T�K��6Cד��GJ����E��}�p�lP�v�ϻۿ���������uE=�P4ME3�R��"�6�"(EP��B�4E�x��P<tR�LE�x��P<����[��R�*
�����B�E�x��P��?��g����V_G|]E�Vk�r�
�+�[��_�	4�@K���2E=�����?�+��+��+�z�/-|��(|u]9��u��Wו_]W|u]9��u�|��ʁ��r��Е_CW|]9�5t���X���0E�k���='��Vţ�WQ(Φ(gW�s(�@S����N_������܊��(���(|��(|��(|-~�-}��(|��(|-S��+
_k+�������e\9=}YW�l(
_6�/[����e�(|�V_t?x��(��MQ�˻�8����s�T�s-E8�)¹�rZ���\\9�G]Eq���P�Q��@8��~�Gq�{>����	����`�]�'�ֻ�SԻ�G�=��{(2k�����g�ѻ�G��O����e�6>}���O_fm�ӗY���e�6>}����Ufm�s�Y��\e�6>W����Ufm�s��]�)�]��em�����)��ϜP�/ks���wm(��w���w���w��xA�*
Ůw-�޵P|Y;R�e�Hŗ�y�����t?��A�(��U�/k�~�/k�~�/kW*��]���v����EEWŭ�toT��B�Y(���B�Y(���BY�����D8}����GmE8*���������ܑ���P��}��a�UO�ǌw�J�6� o������p�N`��2��C���1��@��߂��o�&��-��oy���:���u����O��1���:�7�v����x��:������:���u��Q���^�����x�+g�oC���"�é(n��`�oC�����[�"(^E�� 7��"�x�c�@;�x� �ĳQ�Aq+�b����WQ(2�g���A<s��♣eg�-;�x�g��+��Vţ�W�Oq0����8P4�@S���@��t���A�(��U�♏�� ������@�� ��@PtEP܊�xt1y���7��QVN5���QC�45������������㨣����8�A<�6ān�!(`�A<r�����M��qpp�;r��7�����u�/nzG���ޑC��w��8��94nzG���ޑC��w�#p������U�����ޑ���w�h9��9Znz�L_��
_�􎙾��_G|]Eዛ�@ዛ�@ዛޑq8�����9�nzG����ޑS��w,�:���*
_/k-}������6���6���6���6���6����������6���emn��v�6{0kGn��v�6{0kGn��v�L�vl�:�>_WQ�b֎܌f����`֎܌f�����e�	_��9�Y;r�>�����Q_W���l:����tJ_�Y;���Y;���Y;rc?��#7����f�)��S��K�4�zY�����6����_/k�y?�Ni��uJ���S|u��>_eJ��2������U���Ni���6���em>����|��Q���5ʔ_:�}�tJ�/f����d��|�ϩ�|1kg>��,�B��eR_�ڙ��ɬ����"��������Θ�Ŭ�����ڙ��ɬ�����ڙ���t΁��s��Kg�ϗΘ�Ŭ�����ڙ��i:c��v��~�Θ�e:c�	_�3��״�3�G�s��M҃,!#�	�A\�
��@�|dS�
	�߳�$�~�f�кCHh�)$�nz�nӮ	�����#ZW�O�[z�V�[҃!#�2�,!+�	��~�'�~�7�y���~z~z~���~���X?ޗ|�᧛������[������ڀ�!k~���C���3���3dm�ϐ�?C��Y�Y�Y�3um���k#�LY�3em�ϔ�?S��L~�?���+��Z�		�Յ��BBkM!���bBv��s��d��#~���cMH��.$��~l
	?��sK?fB���c[�!�s��oBw!��sm���SH��%$��		?�Bo!�s���~vzy�݅�y���SΓ�'�cBp��l!8O���k~_!q��k��1��1�6��BpL���cL����go�`���=�{��=�{��=�+� ���ghe6>W3�������+��%�৕������q���l�H2�!+�bA�s>w�`��ą@k���+$��܃��rB�9�R�9�R��sZ]���܃��܃��܃��܃�b�z���\�{�=-��H-��H-�ࠖ����<�u��sp�sp�s0����k3Wj1�\��h!кBB+s0��?҅�V�`��d
	���l��	��ϳt�d����A�#WH9(���y������,��g-��J��~��:�C��l��Ϸ��(�Q�C}%�8���u=
h�Q�.;Q+�?��Y"'�D��������x��~��;��~����o��~��^P�Ϟ����	���e���N���;}���&&��NW�P���7�!���YB�LH���_t�dV��A��#WHheHf/�G���ʐ�V��L!��!9y�dHf%�G\��h!кB~Z'C2��?҅� C�2�� K�
bB�����h!кBB+C2;�?҅�V�d6�d
	������	��������u��V�d6�	������Z����-�-ZG����ʐ̾�t!;���k�,�vp�	�1�9���q�~�8F?�8&C2[�?҅����t2$���#�x ��,�����97�Y���#$>��,f�7On���#]H���b6{d
	?�Y�^���l���?�y��~r�����B�On���#SH���b�yĄ���,f��G��9�|~�������H~r��-��B�On���#&$��f1�?����?WH���b�w�	?�AK?�AK?��s0������0�~���B�s0g���
�`nLs07�'s0��?bB�O�`�ud������
�w.2���#]H�q�9�=��B�=���l���	��@���H�y~�#��+$�df?�G����eF����,��Hz�
�������??��Y����>?2#�ϐ	~��H�����e������gȌ?Cf$�2#}~dF��Ȍ?Sg��3uF
?���9?Sf$��2#�s0�ϗ9��Gg$��)�,���gɌ?��ٿ��)$�,�n�Y���4����솟����
	?��ٻ��.$���u�9���I��|���Y��~2�p�#2o|~dހ��~\�:��̮�L!��e����~\�:�q��>?2�}~d��ʿ�c쇲e�CWQ�z�+{(j�ߥ=�����Z�wq����v*�.�!(�"(nEP<��x�����ѹ��P�CQ(�(�R��A��/�"(EP��B�4E�x��P<峏���>Jܳ��=�(|W��V_�rP�]9X_����nW��_w*
_WW|]]9�uu���Օ��:����U�󵚮�𵚮�𵚮�𵚮�𵚮�𵚮��ZY9'PY9�+��+�����r�E�OE᫧{�M��)��+��Vţ�WQ(���uE;P��ۄkLE�k,E�k���5\Q�[|E�u���Ͼ�����9��9�����i���tE�knE�ŕ���*
_�)
_�+
_k(
_k*
_k)
_���E���ڊp��纊�\F�3�e]Q�ˆ�8�ME8WN���2S�sq�t��(������8�Y˷	��o.f-�&\�Z�M�\�Z<\�Z<\��OQ��OQ�������Z�M���|�pm�k�Ȭ�ۄ�Y˷	��o.f-�&\�Z�M���|�p1k�6�b��m�Ŭ�ۄ�Y˷	��o.f-�&\�Z�M��޵��޵��Qū(�>s�x��ūw-_��T�z�B��]�)�"(nEPԻ�SԻ6�em�
����_���y��ڼ��e���e���+��Vţ�WQ(���_۳���k{�    �6m�^����6�?d��H�Fŭ�G��Pd����<c���+�V��?4�鑵��!S��½��(�K��N�E��ۄ��-oҶ�Mh���6�!o�ۄ��-o��Mh���6�!s�ۄ��-oR��Mh���6�!w�ۄ��-o��u�mB[��Mh��1��c<^�x�:����[�-��:�6���o�����:�6���?��}��z�ڳ]X|ob�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�Mhb�M�b�M�b�M�b�M�b�M�b�M�b�M�b�M�b�M譬UVN� �ۄ� �ۄ��g���g���g���g��\/GmE8�wm>֜A̷	�A̷	�A̷	��^�M����mB禗o:7�|�й��ۄ�M/�&tnz�6�s�˷	��^�M����mB禗o:7�|�й��ۄ�M/�&tnz�6�s�˷	��^�M����mB禗o:7�|�й��ۄ�M/�&tnz�6�s�˷	��^�M����mB禗o:7�|�й��ۄ�M/�&tnz�6�s�˷	��^�M����mB禗o:7�|��_�}]E��em���6�YY�ì���a�_��0�/ks����9���Z��#��u�/f-�&tf-�&tf-�&tf-�&tf-�&tf-�&tf-�&tf-�&tf-�&tf-�&tf-�&tf-�&�S��Ŭ�ۄά�ۄά�ۄά�ۄά�ۄ~tJ�|�_�Z�M��Z�M��Z�M��Z�M�W�4��:����)��Ni�/���/k�y��Ni�k7����n:�����6�����v�)-|�V���Li�Y��Ҭ���S|u����em>����|��^����˔��Ni��uJ�|E�u����|��k�I!|1k�6�f��m�ͬ�ۄ�Y˷	7��o�3��KgL�b��m�ͬ�ۄ�Y˷	7��o�s|M�s�k�	_Sg���}]E�k�	_KgL�Z:c���o�3&|-�1�뗵�C,:�ט��Qū(Y�P(����P�e-PV�����/k
�_�>EWŭ�t�x��7E��]Q(�P�>���}�k㗙��+��Vţ�WQ(������峏��� ��=�����m���֕���u�`}��_|]E��4A�u���ut���ѕ_GW|]9�u\���u�"�ҕ��ҕ_WW|]]9�uu���Օ_����u��	_WW|]]9�/]9�/]9�+~����+�@�Eg���C=�R4�"(�"(nEP<�<�U��?�/:��$~����Շ��է��՗���MQ��(|�����7�����h��������ט���X���0E�kp��O��7��������5���5���5���5���5�~�\��rE8�V�s��乮�8�j��\�+�s-����7�¹�r:�2E8�+g�Gq�Ou�Q�Z��Yˢ�޵�8�Z�������]�)��u��G��Pt�k��z�B�Yˢ�0kYtf-��ìe�y��,:��E�aֲ�<�Z��Yˢ�0kYtf-��ìe�y�޵P�z�~���[���)�3�G�Z(�k�x�������Ü��\�/k��޵��޵��Qū(_��zY�{�e�Lŗ�3_��T|Y;�芠�A������}Y�;���6w�Yˢ�2kYt^fmϽ�e�v��+��Vţ�WQ("k���Z-:/�V�΋�բ�"k���Z-:/�V�΋�-E�Eؖ��"mK�y�����Rt^n):/���[�΋�-E�E薢�"uK�y�����Rt^o):��u�輓����N^��;y�i�:��xב?��s�k�u�輓����V>0^�λx,:���R/X{�
��A̢�2�Yt^1��� f�y�,:/��E�e��b��A̢�2�Yt^1��� f�y�,:/��E�e��b��A̢�2�Yt^1��� f�y�,:/��E�e��b��A̢�2�Yt^1��� f�y�,:/��E�e��b��A̢�2�Yt^1��� f�y�,:/��E�e��b��A̢�2�Yt^1��� f�y�,:/��E�e��b��A̢�2�Yt�[V�*+G��p�U�w��V>{T>{U>{U>{��p�)�Q�k�ǚ5q���YtZcg�i���,:�qӛE�5nz���Mo�ָ�͢�7�YtZ�.���͢�7�YtZ�7�Nk��f�i���,:�qӛE�5nz���Mo�ָ�͢�7�YtZ�7�Nk��f�i���,:�qӛE�5nz���Mo�ָ�͢�7�YtZ�7�Nk��f�i���,:�qӛE�5nz���Mo�ָ�͢�7�YtZ�7�Nk��f�i���,:�����������|]E��e�������^�Z�zYk��e�������^�z�zY��u��U���YtZc�f�i�Y�E�5fm�֘�YtZc�f�i�Y�E�5fm�֘�YtZc�f�im�9'|�2�/fm�֘�YtZc�f�i�Y�E���S|m��>_t��*
_��,:�1k��vtJ�/fm�֎Ni�utJ����|޿��������)��Ni�uuJ���S|������)-|�2������U���S��K����_��󾿬��}oeJ;���H���՛Ni�3k��ά͢�z�I��|�2)��^&��Ŭ͢�:�6�N���,:�3k���uƄ��3���}]E�Y�E�ufm�և�9�Ŭ͢�:�6�N�CgL�b�f�i�Y�E���3��KgL��:c�����Θ�Ŭ͢��������!(�"(nEP<��x��/k�v*����P�e�C���ڇBq-E��L��Pq+��Qū(�)
E�B�����ME�hKQ(�)��+��V�����������&�˻���CQ��(|���/ו���_G�������U����ںr�k�ʁ��+���/-|mS����ںr>_�r>_�r���ʁ��+�NY9�딕����:�r���ʁ��+��u��U�n�o|Nܮ(�P�w*
Ż��5EPtEh+ځ�}���^E?_�uyh�f��h��,�R�L�����9�����!�:���*
_�)
_�+
_}(
_}*
_�+���n��WwE�oE�u��U�FS�FW�ݏ<ט�p���2E8�O�k+¹�"��*�sM������P�kr�t5�(����L�⺟<j+�Q\��$��,:m,�k�	0�޵�K�Z(2k�贱t�Cq��]�"(�]�)�]Efm�6��Yt�`�f�i�Y�E�fm�6��Yt�`�f�i�Y�E�fm�6��Yt�`�f�i�Y�E�׻�/ks�3^��Ng��mT�gΧ�ϜOQ��OQ�Z(n�k�������Z����\�[�Z(n�k?EWŭ�G��P|Y;S�e�Lŗ�3_��T|Y���e-�Ǘ���[�"(^E�Ȭ͢��6�N��,:m0k����͢��6�N��,:m0k�贁�����V�N��Z):m"k�贉����&�V�N��Z):m"k�贉�բ�&�V�N��[-:m"o�贉�բ�&W�N��\-:m"s�贉�բ�&RW�N��]-:m"w�贉�բ�f�u~|��1�;x���1�1x��]G��f+���Ȣ���ud�is����::5&��Sc��l���9��A<��3���� \�,:m2�� �8�N��,:m2���� ΢�&�8�N��,:m2���� ����Yt�dg�i�A�E�Mq�6�Yt�dg�i�A�E�Mq�6�Yt�dg�i�A�E�M��-� ΢�&�x�gg�i�A�E�Mq�6�Yt�dg�i�A�E�Mq�6�Yt�dg�i�A<�hcg�i�A<�pbg�i�A<�hbg�i�A�E�Mq�6�Yt�dg�i�A�E�Mq�6ē�.q�6OY98�����ᨭG��G��>��峏�n��q���QS��]����E�d��b���^���^��    �^���^���^���^���^���^���^���^���^���^���^���^���^���^���^���^���^��oE�E�������M/���M/���M/���M/���M/���M/���M/���M/���M/���M/���M/���M/���M/���M/����6�^������f��Z���������6����6����6Ǒ��6`�^��6{���m�bֲ�\�Z��Yˢs1kYt.f-��Ŭeѹ��,:��E�bֲ�\�Z��Yˢs1kYt.+s|�9'|1kYt.f-��Ŭeѹ��,:��_�S|1kYt.f-��Ŭeѹ��,:��)��Ni�uJ�/f-����6���em>���)��Ni�/��>_:����)��Ni�uʔ�^��y_��J����)��Ni�������u˔�n�����)�^��yϬeѹ��,:��I��N
��2)�W�~��YˢӘ�,:�YˢӘ�,:�������YˢӘ�,:�YˢӘ�,:��_]���:c���E�1kYt��E�u�1�������3&|�1�k�	_��=?Ģ3��C��˭�B�A�Aq+��I��x����|(7�C�8��P�SQ(N��}m�!���芠�A�(��U���s_��P\CQ(��(�R��A�|���V��c}�#�k]E�˚��e]Q�2]9X_�+�˖~��LQ�2_�����ϗ��r]9��r�ˇ~i�˧���r��u����ʁ/ו��ҕ��ҕ_������	_[W|m]9�u���֕_�����W�g�q�P<MQ(��(�P�g*
ųy S��{����������u���u���u���u���u���ϙ�/N=��+
_w+����������Sy��h��ɟF�_�z�Z�F S4��h+���������>�q�8W��\}*¹�~�\��rE8�V�sq���q�8����Y.���C8�+g�Gq�Oe�p�}>ɝYˢӇ޵��wm<|�]ũw-��{(2kYt:��E�3kYt�Ի�SԻ�S��Cū(��,:�Yˢә�,:�Yˢә�,:�Yˢә�,:�Yˢә�,:�Yˢ�M�Z(�޵P|Y�;Y�;7}�@����)�]�)�]�)�]�)�]ŗ�\�/k��]�Z(���݉���݉��TtEP܊�H���WQ(������v���Zޏ/ky?������]TtEP܊�xA�*
Ef-�Ngֲ�tf-�Ngֲ�tf-�Ngֲ�td����բӑ�Zt:�V�NG�j���Z-:Y�E�#kK���Rt:Ҷ���-E�#oK���Rt:���[�΍�-E�F薢s#uKѹ������Rtno):w�u��܍�1�.��E�����:Xt���#z��,���`ѹ;��E��^>0^G���4��l4�ko�^X|ob��A̢s3�Ytn1��� fѹ�,:7��E�f���b��A̢s3�Ytn1��� fѹ�,:7��E�f���b��A̢s3�Ytn1��� fѹ�,:7��E�f���b��A̢s3�Ytn1��� fѹ�,:7��E�f���b��A̢s3�Ytn1��� fѹ�,:7��E�f���b��A̢s3�Ytn1��� fѹ�,:7��E�f���b��A̢s�rpTY98j闆�L�*�=�*�=�*�=�*�}� fѹ�,:7��E�f���b��A̢ss�ˢss�ˢss�ˢss�ˢss�ˢss�ˢss�ˢss�ˢss�ˢss�ˢss�ˢss�ˢss�ˢss�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ�p�ˢ��������M/���M/���6�}^������f���f���f��Z���������6Ǒ�6`z^��6����m�aֲ�<�Z��Yˢ�0kYtf-��ìe�y��,:��E�aֲ�<�Z��Yˢ�0kYt�U��ʜ_G���*
_�Z��Yˢ�Ni�Ŭe�y��,:��E�aֲ�<�Z��tJ�|�_�S|�Ni���6���em>��_�S|�Ni��:�}�tJ�|�_�Li�k�)-|�������uJ���S|�����yY��Li�U���utJ���S|1kYtf-��stR����|�2)��S&�:�>_WQ�bֲ�<�Z���	_�Z��Yˢ�0kYtf-��ìe�y��9�/�s��m:c���t�_�Yˢ�2kYtަ3f��Mg��u�Θ��6�1?_:c~�t���ڿ_���.�����
A=�R4����@[O�Mū(��GA��w?

�=�➊Bq��I�m��芠�A�(��U���o*��(�P�g*
ų��1EP,��	T>����u���������+����+��.�:��5E�� ��[|���|��	_�_����	_�_��/���n ]9�����z ]9������KW��KW|��r�W/+'|u]9��u��Wו_]W|uW��V_���`����+��h�BqtE�8��PSQ(��������������(���(|ͦ(|ͮ(|͡(|ͩ(|�t�E���r���5]Q��[|E�u�����������ܿ_���R��)
_������Q_WQ�2�y.��\6Źl*¹�~�\��rE8�V�sq崼;�*�s9WNϣ�+�Q\9�GME8��~�(S�����In����z��	�z��	�����ֻ�[�=��{(�(�)��޵��޵�"��黯�P<MQ(��(�P�g*
�C�|�SEWŭ�G��P�z�B��]�;�❊B��3�W�9��޵��޵��޵��޵��)Z��"�wm(zӻ6�-E#�)��+��VE��T��B�7E�ػ�P�CQ(��(;ݯT����[�"(^E�Ȭ͢s;�6�����,:�3k���ά͢s;�6����Z):�#k��܎���s;�V����Z):�#k��܎���s;�V���[-:�#m�p��0�Hѹy�E�v��ۑ�ZtnG�jѹ��E�v���ۑ�ZtnG�jѹ��E�v����c��[���ty����0^Ǡ����~z��f�60^G�ۍבE�v����:���u��Q��`��֞���{�6��5�ۿ]o��y�mo2�[+,n�o�,��o�,n�o�^߹���&�o�,t��o��ݣ��ݳ��ݼ�ɻm[a�� ���)����=���>��^X�QX�YX�UX�+��:x�]tOaн���m���텅�}�1S���B���B�Za�� ���>��-짻[+l��`��lf��u|����
���]tOaн��noe�y�^�V����
ǭ�]�8+������������#��\[7za8����L�cv�-e����
��cxa�}�]X|��Ǹ���1�󪧿�s(��9s������.��u�����Ք��ꅅ�5
k��*,�-+,��w3��]�e��[X��VX��^X��QX��YX��w��۬��g��l�0����?o��??�sK���$�sfޒ�94o�󜚷���)�na�O�<g�-y�?�ݒ繫ߒ繭�/ϳY���yV�{�<�nu��Y����<�ս_�g������W�~y���/ϳa���yV�{�<ώu��Y��}�|�N����(��������lZ�~y�U�޷̉�w˜/ϳm���y֭{�<Ͼu��Y��}˜��+sb�;�̉��2'��#y��q$�3?N+sb�;�̉��2'���ʜ��+s��̉����z��_�s��y�̉��˜��G��_��Ή�o�9�F����y6���<�*v�Q��e.��Q��7�\G����^�g#����d��eޅ�Y�]�{y���>/ϳ����y����<�bv�Y��_���o�y�V�w���y����<�zv�U�]�[eޅ�U�]�[e����y��W�ݟ?��)����?h���?U��)-A#PW4E+�Td�V��V�ߟ�EWŭ�G��P�tR�wE�؇�P�SQ(��(�)�"�_*nEP<��x��h    �BqtE�8�g���Z�z��(|W��V_�r:|�ʉ����Y�u�����9�ל���ԕ_SW|M]9�5u�|��~i�u���+����Z�r�k�ʁ��+����Ze儯UV|���|�ʁ/ӕ_�+�l(
_6�/K�Y���e	��+��Vţ�WQ(zS䁺�(�7>M|*
_��/7E��]Q����"������g��������מ���^���6E�k���"�����u��������3��3�����c��ס��smE8�Q�s]Eq�K�3�u��8���\w*¹�rZ���\\9�GmE8�+g��w�b�fI�Y�%�/fm�����Y��jz��`5�k�	��޵��޵����OQ�=��Y��b�fI��]Ůw-��Y��b�fI�Y�%�/fm�����Y��b�fI�Y�%�/fm�����Y��b�fI�Y�%�/fm����޵��޵��Qū(�>s�8��ũw-�޵P�z�Bq�]�)�"(nEPԻ�SԻ�/kG*�����6�������ڙ�/k']�"(EP��B�e�Jŗ�+_֮T|Y�R�Y�W*2k��"��"(EP��B�Y�%�/fm�����R��B�JI�Y+%�/d������R��B�JI�Y+%�/d�����Z��B��ۨ��Z��B�jI���%�/$�����Z��B�jI���%�/������Z��B�jI���%�������cP��:���u����O�]���_�kvyY�����,i}�Q>0^G���u��Q�0W/X{w��x�+ggE�� Ά֍A���8�Y7qֳn�lg��Yκ1���ucg5�� �f֍A�Ŭ�8{Y7qֲn�le��Yʺ1���ucg%�� �F֍A����8�X7qֱn�lc��Yƺ1���ucg�� �&֍A�E��8{X7qְn�la��Yº1���ucg�� �֍A���8�W7q֯n�l_��Y��1��{ucg��� ��ՍA�ū�8{W7q֮n�l]��Y��1��sucg�����Qe��Q��[��Y��Y��qT��qT��qT��q��ᨭG��ǚ1���cr7���4��qи����M��qи�97�#�A�7�U7nz�^u�7�U7nz�\u�7�U7nz�Zu�7�U7nz�Xu�7{U7nz�Vu�7[U7nz�Tu�7;U7nz�Ru�7U7nz�Pu�7�T7nz�Nu�7�T7nz�Lu�7�T7nz�Ju�7�T7nz�Hu�7{T7nz�Fu�7[T7nz�Du�7;T7nz�Bu�7T7nz�@u{Yk�u�|���f�em���6�YY�ì���a�_��0�/ks�N_G���*
_���Lݙ�Y��3k�1ug�fa�ά;ԝY�u�;�6�Rwfm������Jݙ�Y��3k�)ug�fQ�>ʜ�F�s��6[Rwfm������Hݙ�Y����>_:���6�Qwfm֣���lGݙ�Y��O���k�_S�4��:�}��V�����tJ���S|-���k�_K�4�Z:���*SZ�ZeJ�/��ϗf-|�Ni�e:�����|����|޻�)-|Y���_�S���(���(|�N
��:)���I!|y���6�Owfm������>ݙ�Y}��Θ�/�1�Y���;�6kOwfm�����,=ݷ�9�u΁��3&|m�1?_to�u���3&|�1���	_GgL�::c���E�o���6��xAQ����CѮ���CQ����Cѭ���CP�D��[�"(^E��MQ(zW�N���ǿ7���/E�����[��R�*
�����BqE����P�峏>}��>���Ц���"�����ѕ�&�����:C���u���u� �:�(|]9�ut�|�t�|�t���m��������+������r���ʁ��+�����WY9�UV���������xA=�����{^���@����߃=��7�A�(��U��)
���b�<�T��D�4������Wߊ��(���(|��(|��(|�tϷQ��X���0E�k���5�"�:���*
_3��m�;���5���5���5���5MQ���(|ͭ��~�\WQ�k5Eq��Ź��<ך�p���2E8WN�>�][�ŕ�y�UGWN�nٵ�Gq�O5�(��|�_3E�=�޵x�޵x�޵��޵Pt]�Pt]�P��(}*
E׻��w��H���[�"(^E����P�]Q(n��suOE����Pܦ���[�������i�B�tE�x��ţ�(�k�x����������"(^E�x����ջ�w(
�;��]�B�"(����Vţ�Wџ�i�)Z��"D�ߎ�ME=�R4�"(�"(nEP�{��U���,:�PW���,:��T��Z):��)��ý�h+�Q�^��?��<Y+E�#E�d��NE�è#E�ì�=��0��e���N9.>7d���!t���c��[�Q�-�$s�E��-�c�㛼��ty�i�:���uj�w�Oo�X/���Ȣ��:���c�|`���4x�i�����u������qܷ�M7Ϸ�M�;�����&�����&�����
�݅A��[X�z+,t���똼�|��
]� �Aw]^Gv�������{�{��]��m�A���.��0���B��똩{za�{Fa�{fa�{Va�{�0������ {
��-,to+,to/,t�K�;ݻ
�k�A���.����x6�[���~z��ʃ�u��F�.q�,������������ǝrm8���w��3��^�6����YX�
����[�va�=�:�����h��o�������1c���ƻ�̏>va�w�}�na�o����셅�9
s�滎�����+�����S�����j�����]Gfm_���2�[VX�[^X�[�0�;���-,�I�[��<��'y�3s�<ϡ�K����%�sl��97w�s��wN���'y��O������lV��,,��<�n��Ya���y��lG����^�g���za���yv�l�v���߮�U�{y�=�ۅ��)�na��9�N����y��l�^�g��Ǭ��wʜ�̉��2'~�ʜ����s��-s"��2'��-s"��2'��-s"��2'~��uN��5��ߨy�F+sb��̉�oH�g~��̏��x��9�ʜ��+s"��<�&�����_/s	��2��_�sI��u.	/ϳ��c�0�;���-,��2���(�.��<�V���������e���F���o����W���_�w���y����f�w�o�y�f�w�o�y�f�w�o�y����U������'�����wS�������R�����@[�
tY��h���QP��@C�4�@K���CEWŭ�G��P�MQ(v���؇�P�SQ(��(�)��+�b��O����@WP,�ߝ'(|��(|��(|]9=|]9=|ӯ#|W����:��KW|M]9�5u���ԕ_s���R������r�k���|���|�ʁ��+�VY9�k������Z�r�k�ʁ��+��V_G|����?�Ú�P��(m(
E��Bі�P4S�\���ibG|]E�˛���]Q��(|�T�|)
_�����\Q����"�����n���������N�_k�CKQ�ڦ(|mW��V_G|]E��4E�����s��(�u�"�k)¹�~�\��ڊp�����iywܦ(�u�r:��pW��QK�⺟<��(�{>��Q��ջ6� ��]O����������t݇�l��Cq6SEWE�k?E�k?E�ϧ��MQ(��(�P�}*
ž�b��KEWŭ�G��PMQ(�k�8���☊Bq,E�8���)�3�SԻ�SԻ�SԻ�S�Z(ή(�P�S�Z(N�k�8M]�"(EP�����)
�����BqME����P\t��芠�A�(��U��ھR�Y���?4�"��kmh)
Ef�����+�b���E8}���B־�����km(ܿ���f�E�9絶?fd���km��km��������q    H\im����sC�Jk�c��enM����?f�߂9��m��oy���u�^Ǡ��uj^Ǡ�y�Ѿ�t�U����?��hN��g ���O��ѩq����������mz�'~8n7Ϸ�M�����m|��w��|���m}�'y?���}��`��l6���V�U��u�F]/��0� {��
���9m�>
�>ݾ
�n�A��A�Stoa�;Za�;za�;Fa�;�u�����Vt�0�� {
�|���
���9
�9ݹ
���è�Aw�Stoa��Za��zYkl���u����qV�K���}��}��}�qV��8�z�67
�q�>�g�U�f��������va�}�),�����᭰�>�=�z��QX���|��
?߅��)��ud~��
�+��=
{��*,�m+,�m/,��w��Na�w���i����g��,,��UX�;�:2k�������>�0������
���(,�I�[��<��'y�3�<ϡyI��������g��98��y�>&y�?�5���՛�yn�����}��<���������:���|�i���ȟ���?���#
`/�G��^���9��<�� �u���:_��U/�G���^������<�CeN��Q�D�{y>���|�m�����w
��2'��,s"��2'��,s"�I�g~��y��2'��,s"��2'~�ʜ��+s"��2'�ߪsb�[uN5���9��2'��*s"�I�w�;��_�ß�91�Y���ʜ/�g懽<��fe.�?+s	�Y�K��%�w������a/�g懽<���eޅ?/�.��<����|f~�����e���]�+��eޅ�]�]�{y>3?������]�]��eޅ�]���_�w?e��Z���l����5G������CQ�~��h��m�P����VEg��]���ў_s$���[�"(^E�ht��4�k���E�hSQ(�R�f��H���[�"(^E��MQ(zW�^>��E /�}��/A�5 7E��]Q���t�`1�����n�u�����=�מ���֕_[W|m]9�u�|��~i�u���+����:�r���ʁ��+����:e儯SV|���|�ʁ��+�����CQ��SQ���>���A�Aq+��Qū���!��@#Q>M�o�?�-E#�)��\�
���Q_WQ���>_�=�7�
_}(
_}*
_})
_�����Wߊ�+��k�'�F�C�k4E�ktE�kE�kLE�k,E�k���5�~�\[�u�\WQ�k���sͮ(�5��8ל�p.���E����\\9�GmE8�+g�(�Z\�3�Z]���'�\SQ|�K��x̥wm<�һ�SԻ�S�u�)꺇�5E�h]Q(�޵P4�k�ht�O�i����[�"(^E��t����]Q(�P�>��/E���z�~�z�~�G��P��́��g�޵P�z�Bq�]ŭw��芠�AQ��OQ�Z(��(OW�g(
�3������A�Aq+��Qū(/ݯT�]Q(ޡ(�T��ھR�Y��ި�A�(��U�S\�Z�F���|�v!k�5څ���h�V_�]�Z}�v!k�5څ���h�V_�]����Bؖ�hҶ�F���5څ�-��.ny�v!q�k��[^�]����B��hR��F���5څ�-��.oy�v^_�]����h��u����O��1�ƻ��u�5n��`����h��u�5�5G��x|�vM^G�� s���7wa�}����F��]o��ͳ�mo���߾7Y�>��7Y|���7Y�@��w�{�Ѯo��^twa�=�A�����k��za�k��еYX��*,t�
�.����.ۅA��[X�z+,t���������B�Wa��Vt�0�� ��cR������{�{��]��
���]tOaн���yב�Ѯ��3
�3ݳ
�c�A��Z�g�م�`u]Ḻ����w��^��������������rm8n���}�g⽅�~,`�)�矵^X6
�fa3�*l��,�{^�ϟ�]�e��[X�뭰��{aᯏ��_������#�ú��+�����S�����h�����]�Lc��R�
�c�0�������c���s(��9s������.��u����y��&y�C�I���l��96��y��&y���I���c��NG����^��5Z{y��h��9_����|��^��5Z{y��h��9_����|��^��5Z{y��h��9_����|�ּ�W���|�^��5Z{y��h��9_����|�ּ̉��2'���s�Fk/����<�k�����.s"��2'��.s"��2'~�^6�����S�D�;eN��S�D�;eN��S�D�;eN��S���w�%�?%���9�n��O��!y���uN�Ή��9�n�?�0�����y+sI��V����%'X�Kn��,���s�F�/����<�k��ʼ��+�.��<�k������s�F�/����2_�_/���2��_/����]���-,��2���(�.��2���(�.��2���(�.����JI_�!�E��B����/�
�_�[)i��?��(1�]�"(����U��)
�����BqME���>7������+��Vţ�WQ(Z��c�Y��c���EfSQ���(|�)
_�+�t�`}�ѯ�����M|yW�\W|���r]9��r��]����[|���|�ʁ��+����ںr�k�ʁ�]VN��e儯�+����ϗ��ϗ��:MQ�:]Q�:�>��g*
ų��1EPtEP܊�xy������ir���u���u���u���uMQ���(|ݭ��=KZ�W���nM���@C�
4Y�����(ݳ��m+�������՛��ջ��Շ��է����~�\��rE8�V�s��乮�8�h��\�+�s��3���p.��ΣL���<j+�Q\��G]Eq���'��]Q|�S��x�wm<�Ի�S��OQ������S<��x��һ�K�Z(.�ϧ�^SQ(��(�)��+��VE��T��Bњ�P��(m(
E��B������]�)�"(nEP�gΧ��(�޵Pt�k��z�B�����/E���z�~�z�~�G��P�MQ(�(7��fd�(�R��A�Aq+�"�/*^E�x��P<]Q(2k{n-6����b3kY�nf-K�ͬeI���,i7��%�fֲ���Z-i7�VKڍ�Ւv#k����Z-i7�VKڍ�Ւv#kKI�����H�R��m)i򶔴�[Jڃ�-%�A䖒� sKI{���=H�R��n)ir����[J��y,iO�u��=��1��y�i�:��xב?�9}�k�u��=������V>0^K�3x,iϨ��R/��ΰ��{�6�KJ���z��`�0��[���9��7Y|���7�d�/<��7Y�~��d���~�A���.����g��Bw��Bw��Bw��Bw��Bw�:XҞe�A���.��0���B��u�޷^X��(,tm��
]� ��cRw�Stoa�뭰��^X����|�������B׭0�za�݅A�]�Q������{�{�{��Ϧm��`u]Ḻ�p�)�%����q�~qܩ��������[��p����}�g�م�`G����m���q{a�}�QX|w��}ϫ�!��VX�����.�Na�w��������,ػ�̏�fa7�R�n��z0/lۅ��)�na᯿�����_�����__���n�������w��na�o4e�7za�o���ߘ����
�
���$ύ�����-,�I�[��<��'yn�O��ҟ�y�>W�<<{%�sW%ϝ�Na�w/�Y�ޗ�,i��s����9K���%�}yΒ��<gI{_����/�Y�ޗ�,i���*�Y�����s����9K���%�}yΒ�Z���ʜ��{�q���^����/�Y�^/s"�y���˜^�D��<������ʜ��+s"��2'��.s"�� �  2'��.s"��:'��]���W��j��ʜ��+s"�I�3?$ϙ�Ή���91��2'��)s"��<gI{_������%��2��߭sI��u.	/�Y�ޗ�,i��s����9K�{˼�̻��w̏��Y����<K��^�gI{[+����m��W?��2�����ʼ��w���,io{y�%�m�̻��2��_/�.��2��_/�.��2���S�r���ۯ��-�Y���P�ME#�R4���Aq'�T<��x���<��+
�1�����c)
�a��芠�A�(�"��T�MQ(ή(�P�s*
Ź��,��	T>�h��u��U�VS���������5��_k)
_���rE�k���|���|�ʁ/ӕ_��K_6�/ӕ_�+�LW|���2]9�/]9���r���\W|���r]9��r��MQ�rW�<��ݠxA�*
�����BqE���"��@��O����ފ��(���(|��(|��(|��(|�t��?���c���qE��lE�u��U�nS�n������ם���]���5E�뺢�u�"�:�����w�_�-�uEh(ځ�~�\K�e�p.W�sq�����ڂp.���G��(��\9�GE8��~�Gq��|wW��]O�����'��z�Bq�]š��C�=�T�c)
šw���w��H���G��P�MQ(ή(�P�����R��A�Aq+��QE�k�������BqE����ť�(.�k?E�k?E�k?E�k?ū(�)
Eӻ��w-m*
E[�B�L]�~R�(��U����wE��CQ(:ݯT��(�A�Aq+��QE��Td�~��uE�Ȭ�����B�Y�s�������!W�Ӈ�W���Q����+n����?�-"�9����I��0�t�?�Q��?�Y�{|���p�I`��2W���<���ԕ���f��`��Ɍ��[^���wy���:���u��8��1ٻ��M���rm`�����1^Gs2+���?^G�� ��%��魰ב?��q��l��
������`��R��[X�~��d�;za�;Fa�;x3�3Va�;�0�za�݅A�]^��i��VX��^X��QX��YX��UX��w��^twa�=�A�����]�L�5
�5ݵ
�e�A��E�Stoa�k��е^X��(,t�]�����B׬0�za�݅A��[�Z<�����u����q�|�8n������������������n��8n�����s>�(�����۫��>�������ػ��>��U�[��6C�3�IYԋ�K���L����r��v�mVM\��~�W���?o���M����a��)~�����y��p�~�,���h��&~х�/�0��;�^~���/6�����/������u�&l���	�Σ�6�!,�&3�ŷ�5�-,�\X��0��O���g�Q���>��9��kh�󚚃����>��υ��;��}���^��y�����Y�ϭ~���s������}�>��i��m_?�~�,�^�[����Vo����k�x}n� ^�[�������W�{}n~�\X��0��>�ziC���2'���s�������m�>�z��ϭ^0Đ9��ɜx�dNL�)sb�Q�W�y�GL��oʜ�~S����2'�ߔ9��ɜx�tN���9~���}�~K���[2'��y�GP�W��91�tNL?��o˜�~��{�G�>���e.I�-sI�m�K�u.����ޯ�K�������˼�~.�n��>�?^�������������|u�d�:~2�_ȼ�~��������Gȼ�~!�n��̻�2�_ȼ{�d�=~8�������f��䷙��.m����j@�Ȁ�Q��߯�Pc4��Ю��Oꇐ��~���Ge�b���Q&^{�����1B�5FH4c�D댐h�>*�&�L\�2q3�Dg����]����]��d��zg�>��OF��|�4xu�sZz9_��
F������9�5��I��wNz�s�k,�h��Qz�s���I��wNzM�s�k�^����r��kʝ��wNzM�s��9ǋ��Z#x��^��n}����>#$���k2���(7�LtF(��_��m�#xmc���kF�ړ��b���W�����n���c/o����^���#x�d//�Z��~s3J/g�^�^�1�W4F�
c������gMF�Y�Q~�f��u����`���߿7Bh5F��9g��?6B(?��9�5�Q�α{�f�G���ߣ��j���o����O-�V�� ��S����ړ���I���$:�LFH4~j3����D������3B�FH��(�L܌2���MFH�#$����!�wFH���fb��$.F��e"�D�����Om&~j3q�S�����L��d���ԞD~jO�3��`���1B�l��8�}���!qFH��Q&.F��e�71!q}���#$ޮm�o׶Q��kkik�vm-mmݮ����۵���u�����n����Vv--mme����Vv--mme����Vv--mme����Vv--mme����V�-/mme����V�-/mme����V./mme����VV./mme����V�./mme����V�./mme����V//mm�=����a77�y�͈{�2�y��x�q^�؊-��G-mm�=�Z���>�$��QK[��=�Z����z$�{oS���y��<nk`.,���{|���[l�5a�.�߅���-����rϯ�b���e����<jik��0��'�ք!�Lr�C����5mn��2w	��-,s]X�0��w���ބ!��0��.�}Cn��2��G��[X溰�a��0�&��G}��х!wa�SX�.a���e�;�ysCr�'��	C�4aȝ]r�{m�MaL�<N�<��Z�q!�-�8n�����z�qz=�!��May�{��w���̙���
a���뱛0\�m�p=v����U+�=��o/f鷷��sa����0�y?�q�û0��`�~>��ϗ0���~.,�B��G/�h���,���_a��)~���/���{�q�6B��Ͽ���k��	3�.��al
�`�<�Kws��y����a�>��٩�klv�󚛝��g�>��ǩ����S�ׯz�>_�υ�_����Zښ�>������Zښ�>������Zښ�>������Zښ�>������Zښ�>���y��
~]�+��>������Zښ�>������Zښw�ӯ˜x��y��a�{}^K[��絴52'�ߐ91�^����|Ȝ�~���N}�]?��_�ߔ91��̉�7eNL�)sb�Q�W��9~���}~�dN<~!~���N}^��K�D�-��G}^��K���{}^K[��絴5_2����s	���%�{}^K[��絴5}^K[��絴5�2�ߖy�����/����y-m�_�����e�J?��*�^�����e�M��������~��e�=~!~!�n��̻�2�_ȼ�������?w�      c
   s  x�E�Qr�:D��żH����u�n��J����c��ޢ��>Mt�0	� c�t�]��w����z�8^A����i��6T���`vO��m�0���-Y��kl��m$��#��9��T�ט�Ɯ�˘��]Ɩ(# e�D���X*y�e�k�)Y�Z�g�d[N(#�q@m��s��q��r��Kr
������m���$�v�����چQۿM��O���">�9\g����$�v����@-`T�D;	1�W�E|�s��Vi��8DK;��rp��0*i�z�h�"�i��jp*��R��������c���)��8��4�p5��r�TZ�vF�!~��t=���0b=�c�uRt,��:�rq3�� ���v��_Y0��.���yy��������7��������ʃw��o���eUt����2b���N����uN���n�Mr�G�ː�U�B���)�����I.�b��u��.VG�Al7���s�4VqRt�U�mc(E�X�r^��u�l�=���$�{�_���Zg�0�v�zg��	�3;L(�� ZnbqG;�;�b�W ��0m���I���Τ�z���"�W�wV&������ ���      e
   E   x�3��I-K��4�2�L+1��9��S��S��A|����<���˔3)1/;/�$��Ӕ+F��� ~e�      q
   d   x�}˱�@D��Pb�;�
��?R(%���!^B	8w䎱9�,����R���7�ـUL _)��3l�"�|5�0��g�^�d��y��4U� 8f�     