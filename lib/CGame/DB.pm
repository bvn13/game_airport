package CGame::DB;

use strict;
use warnings;

use Rose::DB;
our @ISA = qw(Rose::DB);


__PACKAGE__->use_private_registry;
__PACKAGE__->register_db(
    domain          => '127.0.0.1',
    schema          => 'mgdata',
    type            => 'session',
    driver          => 'Pg',
    host            => '127.0.0.1',
    database        => 'mg',
    username        => 'game',
    password        => 'mygamepass'
);


CGame::DB->default_domain('127.0.0.1');
CGame::DB->default_type('session');

#my $db = Rose::DB->new;
#$db->pg_enable_utf8(1);
#CGame::DB->pg_enable_utf8(1);

1;

