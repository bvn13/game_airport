package CGame::DB::Object;
use CGame::DB;
use base qw(Rose::DB::Object);

sub init_db{ 
    my $db = CGame::DB->new; 
    $db->pg_enable_utf8(1);
    $db->dbh->do{"SET CLIENT_ENCODING TO 'UTF8'"};
    return $db;
};

1;

