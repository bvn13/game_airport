package CGame::DB::ObjectsCatalog;

use base qw(CGame::DB::Object);

__PACKAGE__->meta->setup(
    table => 'objects_catalog',
    auto => 1
);

1;
