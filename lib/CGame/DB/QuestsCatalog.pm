package CGame::DB::QuestsCatalog;

use base qw(CGame::DB::Object);

__PACKAGE__->meta->setup(
    table => 'quests_catalog',
    auto => 1
);

1;
