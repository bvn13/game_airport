package CGame::DB::Users;

use base qw(CGame::DB::Object);

__PACKAGE__->meta->setup(
    table => 'users',
    auto => 1
);

1;
