package CGame::DB::UsersApps;

use base qw(CGame::DB::Object);

__PACKAGE__->meta->setup(
    table => 'users_apps',
    auto => 1
);

1;
