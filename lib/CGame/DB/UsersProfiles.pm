package CGame::DB::UsersProfiles;

use base qw(CGame::DB::Object);

__PACKAGE__->meta->setup(
    table => 'users_profiles',
    auto => 1
);

1;
