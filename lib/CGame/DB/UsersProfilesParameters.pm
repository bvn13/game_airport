package CGame::DB::UsersProfilesParameters;

use base qw(CGame::DB::Object);

__PACKAGE__->meta->setup(
    table => 'users_profiles_parameters',
    auto => 1
);

1;
