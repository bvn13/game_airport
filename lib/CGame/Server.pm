package CGame::Server; {

    use Moo; 

    use utf8;
    use Encode qw/encode decode/;

    use CGame::Server::Errors;
    use CGame::Server::User;
    use CGame::Server::DateTime;
    use CGame::DB;
    use CGame::DB::LogUsersActions;
    use CGame::DB::ObjectsCatalog;
    use CGame::DB::QuestsCatalog;

    use Mojo::JSON;
    use Mojo::Log;

    #use FindBin;
    use Data::Dumper;


    has 'user' => (
        is => 'ro',
        required => '1',
        default => sub {
            new CGame::Server::User;
        }

    );

    has 'log' => (
        is => 'ro',
        required => '1',
        default => sub {
            Mojo::Log->new(path => "/var/log/Mojo/game.log");
        }
    );

    has '_db' => (
        is => 'ro',
        required => '1',
        default => sub {
            CGame::DB->new;
        }
    );

    sub tryLogin {
        my ($self, $login, $pass, $app) = @_;

        #print "LOG " . __PACKAGE__ . "(" . __LINE__ . ") : \n" . Dumper @_;
        $self->log->info("Logging in...");

        return $self->user->error unless $self->user->login($login, $pass, $app);
        my $res = {
            status => $CGame::Server::Errors::_OK
        };
        $res->{status}->{data} = {};
        $res->{status}->{data}->{sid} = $self->user->getSid;
        $res->{status}->{data}->{sid_expiration} = $self->user->getSidExpiration;
        return $res;
    }; # sub tryLogin

    sub restoreSession {
        my ($self, $login, $sid) = @_;

        return {status=>$self->user->error} unless $self->user->loginBySid($login, $sid);
        my $res = {
            status => $CGame::Server::Errors::_OK
        };
        do {
            $self->user->makeSid;
            $res->{status} = $CGame::Server::Errors::_OK_SID_CHANGED;
            print "\n!!! - SID WAS CHANGED - !!\n\n";
        } if !$self->user->checkSid;
        $res->{status}->{data} = {};
        $res->{status}->{data}->{sid} = $self->user->getSid;
        $res->{status}->{data}->{sid_expiration} = $self->user->getSidExpiration;
        return $res;
    }; # sub restoreSession

    sub register {
        my ($self, $login, $pass, $app) = @_;

        return {status=>$self->user->error} unless $self->user->register($login, $pass, $app);
        my $res = {
            status => $CGame::Server::Errors::_OK
        };
        $res->{status}->{data} = {};
        $res->{status}->{data}->{sid} = $self->user->getSid;
        $res->{status}->{data}->{sid_expiration} = $self->user->getSidExpiration;
        #TODO: добавить полный профиль?
        return $res;
    }; # sub register

    sub processUserAction {
        my $self = shift;
        my $action = shift;
        my $data = shift;

        print "LOG: " . __PACKAGE__ . "(" . __LINE__ . "): data: " . Dumper $data;

        #if (! defined $data->{date}) {
        #    my $res = $CGame::Server::Errors::_ERROR_WRONG_DATA_FORMAT;
        #    $res->{comment} = 'Field "Date" is necessary!';
        #    return $res;
        #}

        my $res = {
            status => $CGame::Server::Errors::_OK
        };

        # main method, which doing its job of reinterpreting users` actions
        CGame::DB::LogUsersActions->new(
            user_id => $self->user->getUserId,
            user_app_id => $self->user->getAppId,
            date => CGame::Server::DateTime->now->toStr,
            user_date => $data->{date},
            data_json => Mojo::JSON->new->encode($data)
        )->save;

        print "\n!!!  -  $action\n\n";
        if ($action =~ m/new_game/) {
            # passed
            my $res = $self->newGame;
            return $res if $res->{status}->{code} ne '1';
            $res->{full_state} = $self->getFullUserState;
            return $res;
        } elsif ($action =~ m/get_user_state/) {
            # passed
            return $self->getFullUserState;
        } elsif ($action =~ m{get_quests_available}) {
            # passed
            return $self->getQuestsAvailable;
        } elsif ($action =~ m{get_quests_started}) {
            # passed
            return $self->getQuestsStarted;
        } elsif ($action =~ m{get_quests_ended}) {
            # passed
            return $self->getQuestsEnded;
        } elsif ($action =~ m/create_new_object/) {
            return $self->createNewObject($data);
        } elsif ($action =~ m/start_quest/) {
            return $self->startQuest($data);
        } elsif ($action =~ m/end_quest_with_success/) {
            return $self->endQuest($data, 1);
        } elsif ($action =~ m/end_quest_with_fail/) {
            return $self->endQuest($data, 0);
        } else {
            $res->{status} = $CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG;
            $res->{status}->{text} = "Wrong action!";
        }

        return $res;
    }; # sub processUserAction

    sub getFullUserState {
        #FIXED: переписать на объектную модель
        my $self = shift;

        my $res = {
            status => $CGame::Server::Errors::_OK
        };

        my $qsel = <<EOQSEL;

                select 
                    mgdata.get_objects_catalog() as objcat
                ;
EOQSEL
        my $sel = $self->_db->dbh->prepare($qsel);
        $sel->execute;
        my $qres = $sel->fetchall_arrayref;
        $res->{objects_catalog} = Mojo::JSON->new->decode($qres->[0]->[0])->{objects};

        $res->{profile} = $self->user->profile->getProfile;
        $res->{map} = $self->user->map->getMap;

        return $res;
    }; # sub getFullUserState

    sub newGame {
        #FIXED: переписать на объектную модель
        my $self = shift;

        my $res = {
            status => $CGame::Server::Errors::_OK
        };

        $self->user->profile->generate or do {
            $res->{status} = $CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG;
            $res->{status}->{text} = "Cannot generate new user`s profile!";
        };
        $self->user->map->generate or do {
            $res->{status} = $CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG;
            $res->{status}->{text} = "Cannot generate new user`s map!";
        };

        my $q = $self->_db->dbh->prepare("select mgdata.clear_user_quests(?);");
        $q->bind_param(1, $self->user->getUserId);
        $q->execute;

        return $res;


    }; # sub newGame

    sub getQuestsAvailable {
        my $self = shift;

        my $res = {
            status => $CGame::Server::Errors::_OK
        };

        my $sel = $self->_db->dbh->prepare("select mgdata.get_user_quests_available(?);");
        $sel->bind_param(1, $self->user->getUserId);
        $sel->execute;
        my $rsel = $sel->fetchall_arrayref;
        $res->{questsavailable} = Mojo::JSON->new->decode($rsel->[0]->[0]);

        return $res;
    }; # sub getQuestsAvailable

    sub getQuestsStarted {
        my $self = shift;

        my $res = {
            status => $CGame::Server::Errors::_OK
        };

        my $sel = $self->_db->dbh->prepare("select mgdata.get_user_quests_started(?);");
        $sel->bind_param(1, $self->user->getUserId);
        $sel->execute;
        my $rsel = $sel->fetchall_arrayref;
        $res->{questsstarted} = Mojo::JSON->new->decode($rsel->[0]->[0]);

        return $res;
    }; # sub getQuestsStarted

    sub getQuestsEnded {
        my $self = shift;

        my $res = {
            status => $CGame::Server::Errors::_OK
        };

        my $sel = $self->_db->dbh->prepare("select mgdata.get_user_quests_ended(?);");
        $sel->bind_param(1, $self->user->getUserId);
        $sel->execute;
        my $rsel = $sel->fetchall_arrayref;
        $res->{questsended} = Mojo::JSON->new->decode($rsel->[0]->[0]);

        return $res;
    }; # sub getQuestsEnded

    sub createNewObject {
        my $self = shift;
        my $data = shift;

        my $res = {
            status => $CGame::Server::Errors::_OK
        };
        
        if (!defined $data->{object_id} || !defined $data->{coord_x} || !defined  $data->{coord_y}) {
            $res->{status} = $CGame::Server::Errors::_ERROR_WRONG_DATA_FORMAT;
            return $res;
        }

        my $objData = CGame::DB::ObjectsCatalog->new(id => $data->{object_id});
        if (!$objData->load(speculative => 1)) {
            $res->{status} = $CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG;
            $res->{status}->{text} = "Object was not found in the DB!";
            return $res;
        }
        #FIXED: проверить возможность поставить объект на карту,
        # проверяется в ХП добавления. Возвращается ИД нового объекта. Если 0, то не добавилось.

        my $qtxt = "select mgdata.create_new_user_object(?, ?, ?, ?);";
        print "LOG: " . __PACKAGE__ . "(" . __LINE__ . "): query: " . Dumper($qtxt, $data) . "\n\n";
        my $q = $self->_db->dbh->prepare($qtxt);
        $q->bind_param(1, $self->user->getUserId);
        $q->bind_param(2, $data->{object_id});
        $q->bind_param(3, $data->{coord_x});
        $q->bind_param(4, $data->{coord_y});
        $q->execute or die decode('utf8', @!);;
        my $r = $q->fetchall_arrayref;
        
        if ($r->[0]->[0] > 0) {
            $res->{new_object_id} = $r->[0]->[0];
            $self->user->profile->load ; # если вернулся не ноль, то перечитаем профиль
            $res->{profile} = $self->user->profile->getProfile;
        } else {
            $res->{new_object_id} = $r->[0]->[0];
            $res->{status} = $CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG;
            $res->{status}->{text} = 'Could not place an object on the map!';
        }
        # поставить объект на карту
        # -> ставится запросом выше, вместе с проверкой

        return $res;
    }; # sub createNewObject

    sub startQuest {
        my ($self, $data) = @_;

        my $res = {
            status => $CGame::Server::Errors::_OK
        };

        if (!defined $data->{quest_id}) {
            $res->{status} = $CGame::Server::Errors::_ERROR_WRONG_DATA_FORMAT;
            print "START QUEST fails 1\n";
            return $res;
        }

        my $quest = CGame::DB::QuestsCatalog->new(id => $data->{quest_id});
        if (!$quest->load(speculative => 1)) {
            $res->{status} = $CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG;
            $res->{status}->{text} = "Quest was not found in the DB!";
            print "START QUEST fails 2\n";
            return $res;
        }

        my $qtxt = "select mgdata.start_user_quest(?, ?);";
        my $q = $self->_db->dbh->prepare($qtxt);
        $q->bind_param(1, $self->user->getUserId);
        $q->bind_param(2, $data->{quest_id});
        $q->execute;
        my $r = $q->fetchall_arrayref;

        if ($r->[0]->[0] == 1) {
            $self->user->profile->load;
            $res->{profile} = $self->user->profile->getProfile;
            print "START QUEST success: " . Dumper $res;
        } else {
            $res->{status} = $CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG;
            $res->{status}->{text} = "Could not start quest! Reason: '$r->[0]->[0]'";
            print "START QUEST fails 3\n";
        };

        return $res;
    }; # sub startQuest

    sub endQuest {
        my ($self, $data, $success) = @_;
        
        my $res = {
            status => $CGame::Server::Errors::_OK
        };

        if (!defined $data->{quest_id}) {
            $res->{status} = $CGame::Server::Errors::_ERROR_WRONG_DATA_FORMAT;
            print "END QUEST failed 1\n";
            return $res;
        }

        my $quest = CGame::DB::QuestsCatalog->new(id => $data->{quest_id});
        if (!$quest->load(speculative => 1)) {
            $res->{status} = $CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG;
            $res->{status}->{text} = "Quest was not found in the DB!";
            print "END QUEST failed 2\n";
            return $res;
        }

        my $qtxt = "";
        if ($success) {
            $qtxt = "select mgdata.end_user_quest_with_success(?, ?);";
        } else {
            $qtxt = "select mgdata.end_user_quest_with_fail(?, ?);";
        }
        my $q = $self->_db->dbh->prepare($qtxt);
        $q->bind_param(1, $self->user->getUserId);
        $q->bind_param(2, $data->{quest_id});
        $q->execute;
        my $r = $q->fetchall_arrayref;

        if ($r->[0]->[0] == 1) {
            $self->user->profile->load;
            $res->{profile} = $self->user->profile->getProfile;
            print "END QUEST success\n";
        } else {
            $res->{status} = $CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG;
            $res->{status}->{text} = "Could not end quest! Reason: '$r->[0]->[0]'";
            print "END QUEST failed 3\n";
        };

        return $res;

    }; # sub endQuest
};

1;
