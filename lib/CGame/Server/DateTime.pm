package CGame::Server::DateTime;

use strict;
use warnings;

use DateTime;
use DateTime::Duration;

sub _format {
    my ($self, $dt) = @_;
    return $dt->strftime("%Y-%m-%d %H:%M:%S");
}

sub toStr {
    my $self = shift;
    return $self->_format($self->{dt});
}

sub now {
    my $self = shift;
    return bless {
        dt => DateTime->now(time_zone => 'Europe/Moscow')
    };
}

sub fromStr {
    my ($self, $str) = @_;
    
    return bless {
        dt => DateTime->new(
            year => substr($str, 0, 4),
            month => substr($str, 5, 2),
            day => substr($str, 8, 2),
            hour => substr($str, 11, 2),
            minute => substr($str, 14, 2),
            second => substr($str, 17, 2),
            time_zone => 'Europe/Moscow'
        )
    };

}

sub addDurationHash {
    my ($self, $dh) = @_;

    my %p = ();
    for my $k (keys %$dh) {
        $p{$k} = $dh->{$k} if $dh->{$k} > 0;
    }

    $self->{dt}->add_duration(DateTime::Duration->new(%p));
    return $self;
}


sub compare {
    my ($self, $dt1, $dt2) = @_;
    #print "LOG " . __PACKAGE__ . "(" . __LINE__ . ") comparing dates:\n"
    #    . "\t" . $dt1->toStr . "\n"
    #    . "\t" . $dt2->toStr . "\n";
    #
    return DateTime->compare($dt1->{dt}, $dt2->{dt});
}

1;
