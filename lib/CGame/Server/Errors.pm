package CGame::Server::Errors; 

use strict;
use warnings;

our $_OK = {
    code => 1,
    text => 'ok'
};

our $_ERROR_NONE = {
    code => '0',
    text => ''
};

our $_OK_SID_CHANGED = {
    code => 2,
    text => 'sid changed'
};

our $_ERROR_WRONG_USERNAME = {
    code => '201',
    text => 'Wrong username!'
};

our $_ERROR_WRONG_PASSWORD = {
    code => '202',
    text => 'Wrong password!'
};

our $_ERROR_WRONG_SID = {
    code => '203',
    text => 'Wrong SID!'
};

our $_ERROR_WRONG_DATA = {
    code => '204',
    text => 'Wrong data!'
};

our $_ERROR_WRONG_DATA_FORMAT = {
    code => '205',
    text => 'Wrong data format!'
};

our $_WARNING_SID_EXPIRED = {
    code => '206',
    text => 'Sid changing.'
};

our $_ERROR_SMTH_WAS_WRONG = {
    code => '207',
    text => 'Something was wrong!'
};



1;
