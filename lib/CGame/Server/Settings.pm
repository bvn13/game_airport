package CGame::Server::Settings;

use strict;
use warnings;

our $sid_expiration = {
    years       => 0,
    months      => 0,
    days        => 0,
    hours       => 0,
    minutes     => 1,
    seconds     => 0
};


1;
