package CGame::Server::User; {

    use Moo;
    use strict;
    use warnings;

    use Digest::MD5 qw(md5_hex);

    use CGame::Server::Errors;
    use CGame::Server::DateTime;
    use CGame::Server::Settings;
    use CGame::Server::UserProfile;
    use CGame::Server::UserMap;
    use CGame::DB::Users;
    use CGame::DB::UsersApps;
    use CGame::DB::LogUsersActions;

    use Mojo::Log;
    use Data::Dumper;

    has '_dbt_app' => (
        is => 'rwp',
        #required => '1',
        #default => sub {
        #    new CGame::DB::UsersApps;
        #}
    );

    has '_dbt_user' => (
        is => 'rwp',
        #required => '1',
        #default => sub {
        #    new CGame::DB::Users;
        #}
    );

    has 'profile' => (
        is => 'rwp',
        required => '1',
        default => sub {
            CGame::Server::UserProfile->new
        }
    );

    has 'map' => (
        is => 'rwp',
        required => '1',
        default => sub {
            CGame::Server::UserMap->new
        }
    );

    has 'error' => (
        is => 'rwp',
        required => '1',
        default => sub { return 0; }
    );

    has 'log' => (
        is => 'ro',
        required => '1',
        default => sub {
            Mojo::Log->new(path => '/var/log/Mojo/game.log');
        }
    );

    sub login {
        my ($self, $login, $pass, $app) = @_;

        # making the object of DB::Users
        $self->_set__dbt_user(
            CGame::DB::Users->new(
                login => $login
            )->load(speculative => 1)
        );

        if (! $self->_dbt_user) { # not found
            $self->_set_error($CGame::Server::Errors::_ERROR_WRONG_USERNAME);
            return 0;
        } elsif ($self->_dbt_user->pass ne $self->_makePassHash($login, $pass)) { #wrong password
            $self->_set_error($CGame::Server::Errors::_ERROR_WRONG_PASSWORD);
            return 0;
        };

        $self->profile->_set_user($self);

        $self->_set_error($CGame::Server::Errors::_ERROR_NONE);
        if (! $self->_loadApp($self->_dbt_user->id, $app)) {
            return 0;
        }

        if (! $self->_loadProfile) {
            $self->_set_error($self->profile->error);
            return 0;
        }

        if (! $self->_loadMap) {
            $self->_set_error($self->map->error);
            return 0;
        }

        return $self;
    }; # sub login



    sub _loadApp {
        my ($self, $userid, $app) = @_;

        print "LOG " . __PACKAGE__ . "(" . __LINE__ . ") parameters: " . Dumper ($userid, $app);
        # making the object of DB::UsersApps
        $self->_set__dbt_app(
            CGame::DB::UsersApps->new(
                user_id => $userid,
                app_name => $app
            )->load(speculative => 1)
        );

        my $dt_now = CGame::Server::DateTime->now;

        if (! $self->_dbt_app) {
            # for basic test let's just create application
            $self->_set__dbt_app(
                CGame::DB::UsersApps->new(
                    user_id => $userid,
                    app_name => $app,
                    sid => $self->_makePassHash(
                        $self->_dbt_user->login,
                        $self->_dbt_user->pass,
                        $dt_now->toStr
                    ),
                    sid_creation_date => CGame::Server::DateTime->now->toStr
                )->save
            );
        } elsif ( # if sid was expired
            CGame::Server::DateTime->compare(
                $dt_now,
                CGame::Server::DateTime->fromStr($self->_dbt_app->sid_creation_date)->addDurationHash($CGame::Server::Settings::sid_expiration)
            ) == 1
        ) {
            print "LOG " . __PACKAGE__ . "(" . __LINE__ . ") changind sid!\n";
            $self->makeSid;
        }

        # changing activity
        $self->_dbt_app->last_used(CGame::Server::DateTime->now->toStr);
        $self->_dbt_app->save(changes_only => 1);

        return $self;
        
    }; # sub _loadApp

    sub _loadProfile {
        my $self = shift;
        $self->profile->setUser($self);
        return $self->profile->load;
    }; # sub _loadProfile
    
    sub _loadMap {
        my $self = shift;
        $self->map->setUser($self);
        return $self->map->load;
    }; # sub _loadMap

    sub _makePassHash {
        my ($self, $login, $pass, $salt) = @_;

        if ((! defined $salt || ! $salt) && $self->_dbt_user && $self->_dbt_user->salt) {
            #print "MAKING HASH 1 : " . $self->_dbt_user->salt . "\n";
            return lc(md5_hex(md5_hex($login) . md5_hex($pass) . md5_hex($self->_dbt_user->salt)));
        } elsif (defined $salt && $salt) {
            #print "MAKING HASH 2 : " . $salt . "\n";
            return lc(md5_hex(md5_hex($login) . md5_hex($pass) . md5_hex($salt)));
        } else {
            die "ERROR in " .  __PACKAGE__ . "(" . __LINE__ . ")::_makePassHash : salt is not defined!\n";
            return '';
        }
    }; # sub _makePassHash

    sub _generateSalt {
        my $self = shift;
        my $salt = '';
        for my $c (5 .. 10+rand(5)) {
            $salt .= chr(33+rand(126-33));
        }
        return $salt;
    }; # sub _generateSalt

    sub register {
        my ($self, $login, $pass, $app) = @_;

        # return if user already exist
        my $user = CGame::DB::Users->new(
            login => $login
        )->load(speculative => 1);
        $self->_set_error($CGame::Server::Errors::_ERROR_WRONG_USERNAME) && return 0 if $user;

        my $salt = $self->_generateSalt;
        $self->_set__dbt_user(
            CGame::DB::Users->new(
                login => $login,
                pass => $self->_makePassHash($login, $pass, $salt),
                salt => $salt
            )->save
        );

        $self->profile->_set_user($self);

        $self->_set_error($CGame::Server::Errors::_ERROR_NONE);
        if (! $self->_loadApp($self->_dbt_user->id, $app)) {
            return 0;
        }

        if (! $self->profile->generate) {
            $self->_set_error($self->profile->error);
            return 0;
        }

        return $self;

    }; # sub register

    sub loginBySid {
        my ($self, $login, $sid) = @_;

        $self->_set__dbt_user(
            CGame::DB::Users->new(
                login => $login
            )->load(speculative => 1)
        );
        $self->_set_error($CGame::Server::Errors::_ERROR_WRONG_USERNAME) && return 0 if ! $self->_dbt_user;

        $self->profile->_set_user($self);

        $self->_set__dbt_app(
            CGame::DB::UsersApps->new(
                user_id => $self->_dbt_user->id,
                sid => $sid
            )->load(speculative => 1)
        );
        $self->_set_error($CGame::Server::Errors::_ERROR_WRONG_SID) && return 0 if ! $self->_dbt_app;

        # now it's time for checking sid to be needed for changing
        $self->_set_error($CGame::Server::Errors::_WARNING_SID_EXPIRED) && return 0 unless $sid eq $self->_dbt_app->sid;

        if (! $self->_loadProfile) {
            $self->_set_error($self->profile->error);
            return 0;
        }
        
        if (! $self->_loadMap) {
            $self->_set_error($self->map->error);
            return 0;
        }

        $self->_set_error($CGame::Server::Errors::_ERROR_NONE);
        return $self;
    }; # sub loginBySid

    sub checkSid {
        my $self = shift;
        return CGame::Server::DateTime->compare(
                CGame::Server::DateTime->now,
                CGame::Server::DateTime->fromStr($self->_dbt_app->sid_creation_date)->addDurationHash($CGame::Server::Settings::sid_expiration)
            ) <= 0
        ;
    }; #sub checkSid

    sub getSid {
        my $self = shift;
        return $self->_dbt_app->sid;
    }; # sub getSid

    sub makeSid { 
        my $self = shift;
        my $dt_now = CGame::Server::DateTime->now;

        $self->_dbt_app->sid(
            $self->_makePassHash(
                $self->_dbt_user->login,
                $self->_dbt_user->pass,
                $dt_now->toStr
            )
        );
        # and its creation date
        $self->_dbt_app->sid_creation_date(
            CGame::Server::DateTime->now->toStr
        );
        $self->_dbt_app->save(changes_only => 1);
    }; # sub makeSid

    sub getSidExpiration {
        my $self = shift;
        return CGame::Server::DateTime->fromStr($self->_dbt_app->sid_creation_date)->addDurationHash($CGame::Server::Settings::sid_expiration)->toStr;
    }; # sub getSidExpiration
    
    sub getUserId {
        my $self = shift;
        return $self->_dbt_user->id;
    }; # sub getUserId

    sub getAppId {
        my $self = shift;
        return $self->_dbt_app->id;
    }; # sub getAppId

};

1;
