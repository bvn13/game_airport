package CGame::Server::UserMap; {

    use strict;
    use warnings;

    use Moo;
    use Mojo::JSON;

    use CGame::DB;
    use CGame::Server::Errors;
    use CGame::Server::User;

    has '_data' => (
        is => 'rwp',
        required => '1',
        default => sub { {}; }
    );

    has '_db' => (
        is => 'rwp',
        required => '1',
        default => sub {
            CGame::DB->new;
        }
    );

    has 'error' => (
        is => 'rwp',
        required => '1',
        default => sub {
            $CGame::Server::Errors::_ERROR_NONE;
        }
    );

    has 'user' => (
        is => 'rwp',
    );

    sub setUser {
        my $self = shift;
        $self->_set_user(shift);
    }; # sub setUser

    sub load {
        my $self = shift;

        my $dbh = $self->_db->dbh;
        my $sel = $dbh->prepare("select mgdata.get_user_objects(?) as map;");
        $sel->bind_param(1, $self->user->getUserId);
        $sel->execute;
        my $selRes = $sel->fetchall_arrayref;
        my $json = Mojo::JSON->new;
        my $map = $json->decode($selRes->[0]->[0]);
        if ($json->error) {
            $self->_set_error($CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG);
            $self->error->{text} = "User map is empty!";
            return 0;
        } else {
            $self->_set__data($map);
        }

        return $self;

    }; # sub load

    sub generate {
        my $self = shift;

        my $dbh = $self->_db->dbh;
        $self->_db->do_transaction(
            sub {
                my $sel = $dbh->prepare("select mgdata.generate_new_user_map(?);");
                $sel->bind_param(1, $self->user->getUserId);
                $sel->execute;
            }
        ) or do {
            $self->_set_error($CGAme::Server::Errors::_ERROR_SMTH_WAS_WRONG);
            $self->error->{text} = "Cannot generate new new!";
            return 0;
        };

        return 1;
    }; # sub generate

    sub getMap {
        return shift->_data;
    }; # sub getMap


};

1;
