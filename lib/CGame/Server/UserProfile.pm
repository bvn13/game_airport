package CGame::Server::UserProfile; {

    use Moo;
    use CGame::DB;
    use Mojo::JSON;
    use CGame::Server::Errors;

    use Data::Dumper;

    has '_data' => (
        is => 'rwp',
        required => '1',
        default => sub {
            {};
        }
    );

    has '_db' => (
        is => 'rwp',
        required => '1',
        default => sub {
            CGame::DB->new;
        }
    );

    has 'error' => (
        is => 'rwp',
        required => '1',
        default => sub {
            $CGame::Server::Errors::_ERROR_NONE;
        }
    );

    has 'user' => (
        # must be predefined via constructor
        is => 'rwp',
        #required => '1'
    );

    sub setUser {
        my $self = shift;
        $self->_set_user(shift);
    };

    sub load {
        my $self = shift;
        
        print "LOG: " . __PACKAGE__ . "(" . __LINE__ . "): user_id = " . $self->user->getUserId . "\n";
        my $dbh = $self->_db->dbh;
        #$self->_db->do_transaction(
        #    sub {
                my $sel = $dbh->prepare("select mgdata.get_user_profile(?) as profile;");
                $sel->bind_param(1, $self->user->getUserId);
                $sel->execute;
                my $selRes = $sel->fetchall_arrayref;
                print "LOG: " . __PACKAGE__ . "(" . __LINE__ . "): arrayref: " . Dumper $selRes; #$sel->fetchall_arrayref;
                my $json = Mojo::JSON->new;
                my $profile = $json->decode($selRes->[0]->[0]); #$sel->fetchall_arrayref->[0]->[0]);
                if ($json->error) {
                    $self->_set_error($CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG);
                    $self->error->{text} = "User profile is empty!";
                    return 0;
                } else {
                    $self->_set__data($profile);
                }
                #) or do {
                #    $self->_set_error($CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG);
                #    $self->error->{text} = "Cannot retreive user profile!";
                #    return 0;
                #};
                
        return $self;
    }; # sub load

    sub generate {
        my $self = shift;

        my $dbh = $self->_db->dbh;
        $self->_db->do_transaction(
            sub {
                my $sel = $dbh->prepare("select mgdata.generate_new_user_profile(?);");
                $sel->bind_param(1, $self->user->getUserId);
                $sel->execute;
            }
        ) or do {
            $self->_set_error($CGame::Server::Errors::_ERROR_SMTH_WAS_WRONG);
            $self->error->{text} = "Cannot generate new profile!";
            return 0;
        };

        return 1;
    }; # sub generate

    sub save {
        my $self = shift;
        #TODO: написать метод сохранения профиля в БД
    }; # sub save

    sub val {
        my ($self, $name) = @_;

        return $self->_data->{$name}->{value};
    }; # sub val

    sub getProfile {
        return shift->_data;
    }; # sub getProfile

};

1;
