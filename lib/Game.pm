package Game;
use Mojo::Base 'Mojolicious';

#sub new {
#    return shift;
#}

# This method will run once at server start
sub startup {
    my $self = shift;

    $self->secret('KTqXuqkGQ8o6J2yg');

    # Documentation browser under "/perldoc"
    $self->plugin('PODRenderer');

    # Router
    my $r = $self->routes;

    # Normal route to controller
    $r->get('/')->to('example#welcome');

    $r->get('/register')
        ->to('actions#register')
    ;
    $r->get('/login')
        ->to('actions#userlogin')
    ;
    $r->get('/do/:actionname')
        ->via('post')
        ->to('do#action')
    ;
    #$r->get('/userstate')
    #    ->to('actions#userstate')
    #;
    #$r->get('/newgame')
    #    ->to('actions#newgame')
    #;
    #$r->get('/getquestsavail')
    #    ->via('post')
    #    ->to('actions#getquestsavail')
    #;
    #$r->get('/getquestsactive')
    #    ->via('post')
    #    ->to('actions#getquestsactive')
    #;
    #$r->get('/getquestssuccess')
    #    ->via('post')
    #    ->to('actions#getquests')
    #;
    #$r->get('/getquestsfail')
    #    ->via('post')
    #    ->to('actions#getquestsfail')
    #;
}

1;
