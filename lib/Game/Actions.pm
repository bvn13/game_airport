package Game::Actions;
use Mojo::Base 'Mojolicious::Controller';

use CGame::Server;

use Data::Dumper;

sub _restoreSession {
    my ($self, $server, $res_total) = @_;
    
    #my $server = CGame::Server->new;

    my $res_restoring = $server->restoreSession($self->param('login'), $self->param('sid'));
    if ($res_restoring->{status}->{code} != 1 && $res_restoring->{status}->{code} != 2) {
        return $self->render(
            template => 'errors/something_was_wrong',
            result => $res_restoring
        );
        #die "ERROR: " . __PACKAGE__ . "(" . __LINE__ . "): " . Dumper $res_restoring;
    };

    #do {
    #    $server->user->makeSid;
    #    $res_total->{sid} = $server->user->getSid;
    #} if !$server->user->checkSid;
}
 



sub register {
    my $self = shift;

    my $server = CGame::Server->new;

    #TODO: добавить проверку на допустимые логин и пароль

    $self->render(
        result => $server->register(
            defined $self->param('login') ? $self->param('login') : '', 
            defined $self->param('pass')  ? $self->param('pass')  : '', 
            defined $self->param('app')   ? $self->param('app')   : ''
        )
    );

}

sub userlogin {
    my $self = shift;

    my $server = CGame::Server->new;

    #print "LOG " . __PACKAGE__ . "(" . __LINE__ . ") app = " . $self->param('app') . "\n";
    # Render template "example/welcome.html.ep" with message
    $self->render(
        result => $server->tryLogin(
            defined $self->param('login') ? $self->param('login') : '', 
            defined $self->param('pass')  ? $self->param('pass')  : '', 
            defined $self->param('app')   ? $self->param('app')   : ''
        )
    );
}


sub gameaction {
    my $self = shift;

    my $actName = $self->stash('gameactionname');
    my $data = $self->req->json;

    my $server = CGame::Server->new;
    my $res_total = {};
    
    $self->_restoreSession($server, $res_total);

    #print "LOG " . __PACKAGE__ . "(" . __LINE__ . "): gameaction params: " . Dumper $self->req->json;

    $res_total->{data} = $server->processUserAction($data);

    #print "LOG " . __PACKAGE__ . "(" . __LINE__ . "): gameaction result: " . Dumper $res_total->{data};

    # normal result
    $self->render(
        result => $res_total
    );
}

sub userstate {
    my $self = shift;

    my $server = CGame::Server->new;
    my $res_total = {};
    $self->_restoreSession($server, $res_total);

    $res_total->{fullstate} = $server->getFullUserState;
    if ($res_total->{fullstate}->{status}->{code} ne '1') {
        return $self->render(
           template => 'errors/something_was_wrong',
           result => $res_total->{fullstate}->{status}
       );
    }
    $res_total->{status} = $res_total->{fullstate}->{status};
    delete $res_total->{fullstate}->{status};

    $self->render(
        result => $res_total
    );
}

sub newgame {
    my $self = shift;

    my $server = CGame::Server->new;
    my $res_total = {};
    $self->_restoreSession($server, $res_total);

    my $res_newGame = $server->newGame;
    foreach(keys %$res_newGame) {
        $res_total->{$_} = $res_newGame->{$_};
    }

    $self->render(
        result => $res_total
    );

}


sub getquestsavail {
    my $self = shift;

    my $server = CGame::Sever->new;
    my $res_total = {};
    $self->restoreSession($server, $res_total);

    $self->render(
        template => 'actions/gameaction',
        result => $res_total
    );
}
sub getquestsactive {
    my $self = shift;

    my $server = CGame::Sever->new;
    my $res_total = {};
    $self->restoreSession($server, $res_total);

    $self->render(
        template => 'actions/gameaction',
        result => $res_total
    );
}
sub getquestssuccess {
    my $self = shift;

    my $server = CGame::Sever->new;
    my $res_total = {};
    $self->restoreSession($server, $res_total);

    $self->render(
        template => 'actions/gameaction',
        result => $res_total
    );
}
sub getquestsfail {
    my $self = shift;

    my $server = CGame::Sever->new;
    my $res_total = {};
    $self->restoreSession($server, $res_total);

    $self->render(
        template => 'actions/gameaction',
        result => $res_total
    );
}



1;
