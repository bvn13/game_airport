package Game::Do;
use Mojo::Base 'Mojolicious::Controller';

use CGame::Server;

use Data::Dumper;

sub action {
    my $self = shift;
    my $actName = $self->stash('actionname');

    my $data = $self->req->json;
    print Dumper $self->req->json;
    
    my $server = CGame::Server->new;
    my $res = $server->restoreSession($self->param('login'), $self->param('sid'));
    if ($res->{status}->{code} != 1 && $res->{status}->{code} != 2) {
        return $self->render(
            template => 'errors/something_was_wrong',
            result => $res
        );
    }

    $res = $server->processUserAction($actName, $data);
    print "\nRESULT: " . Dumper($res) . "\n\n";
    $self->render(
        result => $res
    );
}
 

1;
