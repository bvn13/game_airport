use Mojo::Base -strict;

use Test::More tests => 3;
use Test::Mojo;

my $t = Test::Mojo->new('Game');
say "Login status: " . $t->get_ok('/login.json?login=test2&pass=pass&app=android')
    ->status_is(200)
#    ->content_like(qr/Mojolicious/i)
    ->json_is('/result/code' => 1)
;
