use strict;
use warnings;


use FindBin;
use lib "$FindBin::Bin/../../lib";

use CGame::Server::DateTime;
use CGame::Server::Settings;

print CGame::Server::DateTime->now->toStr . "\n\n";

print CGame::Server::DateTime->compare(
    CGame::Server::DateTime->now,
    CGame::Server::DateTime->fromStr(CGame::Server::DateTime->now->toStr)->addDurationHash($CGame::Server::Settings::sid_expiration)
)
            . "\n\n";
