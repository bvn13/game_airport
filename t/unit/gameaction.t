use strict;
use warnings;

use Mojo::UserAgent;
use Mojo::JSON;

use DateTime;

use Data::Dumper;
sub dmp { print Dumper @_; }
sub logit { print shift . Dumper @_; }

my $ua = Mojo::UserAgent->new;
my $sid = '';
my $login = 'test3';
my $pass = 'pass';
my $app = 'a';

sub now {
    return DateTime->now(time_zone => 'Europe/Moscow');
}

sub getLoginPass {
    print "Input LOGIN: ";
    $login = <>; chomp $login; print "\n\tLOGIN: <$login>\n";
    print "Input PASS: ";
    $pass = <>; chomp $pass; print "\n\tPASS: <$pass>\n";
    print "Input APP: ";
    $app = <>; chomp $app; print "\n\tAPP: <$app>\n";
    print "\n\n";
}

sub test_registration {
    print "\n00-registration...\n";
    my $regRes = $ua->get("http://localhost:3000/register.json?login=$login&pass=$pass&app=$app")->res->dom->text;
    my $regJSON = Mojo::JSON->new->decode($regRes);

    #die "FAILED!\n" . $regJSON->error if $regJSON->error;

    logit "Registration responce:\n", $regRes, $regJSON;
    
    die "FAILED!\n" unless defined $regJSON;
    die "FAILED!\n" unless $regJSON->{result}->{code} == 1;
    $sid = $regJSON->{result}->{data}->{sid};
    print "PASSED.\n";
}

sub test_login {
    print "\n01-login...\n";
    my $loginRes = $ua->get("http://localhost:3000/login.json?login=$login&pass=$pass&app=$app")->res->dom->text; #to_xml;
    my $loginJSON = Mojo::JSON->decode($loginRes);
    logit "Login responce:\n", $loginRes, $loginJSON;

    die "FAILED!\n" unless defined $loginJSON;
    die "FAILED!\n" unless $loginJSON->{result}->{code} == 1;
    $sid = $loginJSON->{result}->{data}->{sid};
    print "PASSED.\n";
}

sub test_action1 {
    print "\n02-action...\n";
    print "sid=$sid\n";

    my $test = {
        date => now,
        a => 'b'
    };

    my $tx = $ua->post("http://localhost:3000/gameaction.json?login=$login&sid=$sid"
        #=> {DNT => 1} 
        => {}
        => json => $test);
    my $res = $tx->res->dom->text;
    die "ERROR: " . $tx->error . "\n\n" . $res if $tx->error;
    logit "Action responce:\n", $res;
    print "PASSED.\n";
}

sub test_generate_new_map {
    print "\n03-generate_new_map...\n";
    print "sid=$sid\n";

    my $tx = $ua->post("http://localhost:3000/gameaction.json?login=$login&sid=$sid"
        => {}
        => json => {
            date => now,
            new_game => 1
        }
    );
    my $res = $tx->res->dom->text;
    die "ERROR: " . $tx->error . "\n\n" . $res if $tx->error;
    logit "Action responce:\n", $res;
    print "PASSED.\n";
}

sub test_get_full_state {
    print "\n04-get_full_state...\n";
    
    my $tx = $ua->get("http://localhost:3000/userstate.json?login=$login&sid=$sid");
    my $res = $tx->res->dom->text;
    die "ERROR: " .$tx->error . "\n\n" . $res if $tx->error;
    logit "Action responce:\n", Mojo::JSON->new->decode($res);
    print "PASSED.\n";
}

sub main {
    getLoginPass;
    test_registration;
    test_login;
    test_action1;
    test_generate_new_map;
    test_get_full_state;
}

main;
