use strict;
use warnings;

use Mojo::UserAgent;
use Mojo::JSON;

use DateTime;

use Data::Dumper;
sub dmp { print Dumper @_; }
sub logit { print shift . Dumper @_; }

my $ua = Mojo::UserAgent->new;
my $sid = '';
my $login = 'test3';
my $pass = 'pass';
my $app = 'a';

sub now {
    return DateTime->now(time_zone => 'Europe/Moscow');
}

sub getLoginPass {
    print "Input LOGIN: ";
    $login = <>; chomp $login; print "\n\tLOGIN: <$login>\n";
    print "Input PASS: ";
    $pass = <>; chomp $pass; print "\n\tPASS: <$pass>\n";
    print "Input APP: ";
    $app = <>; chomp $app; print "\n\tAPP: <$app>\n";
    print "\n\n";
}

sub test_registration {
    print "\n00-registration...\n";
    my $regRes = $ua->get("http://localhost:3000/register.json?login=$login&pass=$pass&app=$app")->res->dom->text;
    my $regJSON = Mojo::JSON->new->decode($regRes);

    #die "FAILED!\n" . $regJSON->error if $regJSON->error;

    logit "Registration responce:\n", $regRes, $regJSON;
    
    die "FAILED!\n" unless defined $regJSON;
    die "FAILED!\n" unless $regJSON->{result}->{code} == 1;
    $sid = $regJSON->{result}->{data}->{sid};
    print "PASSED.\n";
}

sub test_login {
    print "\n01-login...\n";
    my $loginRes = $ua->get("http://localhost:3000/login.json?login=$login&pass=$pass&app=$app")->res->dom->text; #to_xml;
    my $loginJSON = Mojo::JSON->decode($loginRes);
    logit "Login responce:\n", $loginRes, $loginJSON;

    die "FAILED!\n" unless defined $loginJSON;
    die "FAILED!\n" unless $loginJSON->{result}->{status}->{code} == 1;
    $sid = $loginJSON->{result}->{status}->{data}->{sid};
    print "PASSED.\n";
}

sub test_action1 {
    print "\n02-action get_quests_available...\n";
    print "sid=$sid\n";

    my $test = {
        date => now
    };

    my $tx = $ua->post("http://localhost:3000/do/get_quests_available.json?login=$login&sid=$sid"
        #=> {DNT => 1} 
        => {}
        => json => $test);
    my $res = $tx->res->dom->text;
    die "ERROR: " . $tx->error . "\n\n" . Dumper $res if $tx->error;
    logit "Action responce:\n", $res;
    print "PASSED.\n";
}

sub test_action2 {
    print "\n03-action get_quests_started...\n";
    print "sid=$sid\n";

    my $test = {
        date => now
    };

    my $tx = $ua->post("http://localhost:3000/do/get_quests_started.json?login=$login&sid=$sid"
        #=> {DNT => 1} 
        => {}
        => json => $test);
    my $res = $tx->res->dom->text;
    die "ERROR: " . $tx->error . "\n\n" . Dumper $res if $tx->error;
    logit "Action responce:\n", $res;
    print "PASSED.\n";
}

sub test_action3 {
    print "\n04-action get_quests_ended...\n";
    print "sid=$sid\n";

    my $test = {
        date => now
    };

    my $tx = $ua->post("http://localhost:3000/do/get_quests_ended.json?login=$login&sid=$sid"
        #=> {DNT => 1} 
        => {}
        => json => $test);
    my $res = $tx->res->dom->text;
    die "ERROR: " . $tx->error . "\n\n" . Dumper $res if $tx->error;
    logit "Action responce:\n", $res;
    print "PASSED.\n";
}

sub test_action4 {
    print "\n05-action get_user_state...\n";
    print "sid=$sid\n";

    my $test = {
        date => now
    };

    my $tx = $ua->post("http://localhost:3000/do/get_user_state.json?login=$login&sid=$sid"
        #=> {DNT => 1} 
        => {}
        => json => $test);
    my $res = $tx->res->dom->text;
    die "ERROR: " . $tx->error . "\n\n" . Dumper $res if $tx->error;
    logit "Action responce:\n", $res;
    print "PASSED.\n";
}

sub test_action5 {
    print "\n06-action new_game...\n";
    print "sid=$sid\n";

    my $test = {
        date => now
    };

    my $tx = $ua->post("http://localhost:3000/do/new_game.json?login=$login&sid=$sid"
        #=> {DNT => 1} 
        => {}
        => json => $test);
    my $res = $tx->res->dom->text;
    die "ERROR: " . $tx->error . "\n\n" . Dumper $res if $tx->error;
    logit "Action responce:\n", $res;
    print "PASSED.\n";
}

sub test_action6 {
    print "\n07-action create_new_object...\n";
    print "sid=$sid\n";

    my $test = {
        date => now,
        object_id => 5, # hangar
        coord_x => 20,
        coord_y => 8
    };

    my $tx = $ua->post("http://localhost:3000/do/create_new_object.json?login=$login&sid=$sid"
        #=> {DNT => 1} 
        => {}
        => json => $test);
    my $res = $tx->res->dom->text;
    die "ERROR: " . $tx->error . "\n\n" . Dumper $res if $tx->error;
    logit "Action responce:\n", $res;
    print "PASSED.\n";
}

sub test_action7 {
    print "\n08-action start_quest...\n";
    my $test = {
        date => now
    };

    my $tx = $ua->post("http://localhost:3000/do/get_quests_available.json?login=$login&sid=$sid"
        => {}
        => json => $test);
    my $res = $tx->res->json;

    print Dumper($res);

    die "ERROR: " . $tx->error ."\n\n" if $tx->error;
    die "ERROR: " . $res->{result}->{status}->{text} unless $res->{result}->{status}->{code} == 1 || $res->{result}->{status}->{code} == 2;
    do {$sid = $res->{result}->{status}->{data}->{sid}; print "NEW SID: $sid\n";} if $res->{result}->{status}->{code} == 2;

    print "SID: $sid\n";

    my @quest_id = keys %{$res->{result}->{questsavailable}};
    print "ARRAY QUEST ID: " . Dumper \@quest_id;
    $test = {
        date => now,
        quest_id => (keys %{$res->{result}->{questsavailable}})[0]
    };

    #print "Data for starting quest: " .Dumper $test;

    $tx = $ua->post("http://localhost:3000/do/start_quest.json?login=$login&sid=$sid"
        => {}
        => json => $test);
    $res = $tx->res->json;

    print Dumper($res);

    print "PASSED.\n";
}

sub test_action8 {
    print "\n09-action end_quest...\n";
    my $test = {
        date => now
    };

    my $tx = $ua->post("http://localhost:3000/do/get_quests_started.json?login=$login&sid=$sid" => {} => json => $test);
    my $res = $tx->res->json;

    print Dumper($res);

    die "ERROR: " . $tx->error . "\n\n" if $tx->error;
    die "ERROR: " . $res->{result}->{status}->{text} unless $res->{result}->{status}->{code} == 1 || $res->{result}->{status}->{code} == 2;
    $sid = $res->{result}->{status}->{data}->{sid} if $res->{result}->{status}->{code} == 2;

    print "SID: $sid\n";
    $test = {
        date => now,
        quest_id => (keys %{$res->{result}->{questsstarted}})[0]
    };

    $tx = $ua->post("http://localhost:3000/do/end_quest_with_success.json?login=$login&sid=$sid" => {} => json => $test);
    $res = $tx->res->json;

    print Dumper($res);

    print "PASSED.\n";
}


#######################################
sub test_generate_new_map {
    print "\n03-generate_new_map...\n";
    print "sid=$sid\n";

    my $tx = $ua->post("http://localhost:3000/gameaction.json?login=$login&sid=$sid"
        => {}
        => json => {
            date => now,
            new_game => 1
        }
    );
    my $res = $tx->res->dom->text;
    die "ERROR: " . $tx->error . "\n\n" . Dumper $res if $tx->error;
    logit "Action responce:\n", $res;
    print "PASSED.\n";
}

sub test_get_full_state {
    print "\n04-get_full_state...\n";
    
    my $tx = $ua->get("http://localhost:3000/userstate.json?login=$login&sid=$sid");
    my $res = $tx->res->dom->text;
    die "ERROR: " .$tx->error . "\n\n" . $res if $tx->error;
    logit "Action responce:\n", Mojo::JSON->new->decode($res);
    print "PASSED.\n";
}



##################################################
sub main {
    getLoginPass;
    #test_registration;
    test_login;
    test_action1; # get quests available
    #test_action2; # get quests started
    #test_action3; # get quests ended
    test_action4; # get user state
    test_action5; # new game
    #test_action6; # quest new object
    test_action7; # start quest
    test_action2;
    test_action8; # end quest
    test_action3;
    #test_generate_new_map;
    #test_get_full_state;
}

main;
